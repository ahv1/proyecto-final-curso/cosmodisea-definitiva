<?php

use app\models\clases;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clases-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Clases', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <style>
        .btn.btn-success {
        /* tamaño del botón */
            padding: 10px 20px; 
            font-size: 1em; 
            border: none;
            border-radius: 20px;
            background-color: #AAD399;
            color: black; 
            display: inline-block;
            cursor: pointer;
            box-shadow: 0 4px #759e64; 
            transition-duration: 0.4s;
        }

        .btn.btn-success:hover {
            background-color: #F5DF61;
            box-shadow: 0 4px #c9b649;
        }

        .btn.btn-success:active {
            background-color: #3e8e41; 
            box-shadow: 0 2px #666; 
            transform: translateY(2px); 
        }
    </style>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'n_jugador',
            'clase',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, clases $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
