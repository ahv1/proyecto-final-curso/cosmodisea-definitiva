<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\clases $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clases-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'n_jugador')->dropDownList(
        $model->getTripulantesList(),
        ['prompt' => 'Seleccione un Jugador']
    ) ?>
    
    <?= $form->field($model, 'clase')->dropDownList(// Establezco que el desplegable tenga estas opciones
        ['mercenario' => 'Mercenario', 'tecnico' => 'Técnico', 'ranger' => 'Ranger', 'sanador' => 'Sanador'],
        ['prompt' => 'Seleccione una Clase']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
