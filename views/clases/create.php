<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\clases $model */

$this->title = 'Create Clases';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    
    
    
    
    
    <!-- BOTON MASCOTA -->
<?= Html::a('?', [''], ['class' => 'btn btn-primary btn-fijo', 'id' => 'boton-toggle']) ?>



<!-- CONTENEDOR MASCOTA -->
<div id="imagen-container" style="display: none; position: fixed; bottom: 100px; right: 30px; z-index: 101; background: 000;">
    <img class="d-block w-1" src="<?= Yii::getAlias('@web')?>/img/ATH/ATH_CLASES.png" style="width: 500px; height: auto;">
</div>




<!-- JS MANEJAR MASCOTA -->
<script>
document.getElementById('boton-toggle').addEventListener('click', function(event) {
    // Previene el comportamiento por defecto del enlace
    event.preventDefault();
    
    var boton = this;
    var imagenContainer = document.getElementById('imagen-container');
    
    // Verifica si la imagen ya está visible
    if (imagenContainer.style.display === 'none') {
        // Muestra la imagen
        imagenContainer.style.display = 'block';
        
        // Cambia la apariencia del botón a 'X'
        boton.innerHTML = 'X';
    } else {
        // Oculta la imagen
        imagenContainer.style.display = 'none';
        
        // Restablece la apariencia del botón a '?'
        boton.innerHTML = '?';
    }
});
</script>

</div>
