<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\objetos $model */

$this->title = 'Update Objetos: ' . $model->cod_items;
$this->params['breadcrumbs'][] = ['label' => 'Objetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_items, 'url' => ['view', 'cod_items' => $model->cod_items]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="objetos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
