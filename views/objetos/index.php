<?php

use app\models\objetos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider 

$this->title = 'Objetos';
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="objetos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <div class="container mt-5">
        <div class="bi-exp">
            <img src="<?= Yii::getAlias('@web') ?>/img/tienda.jpg" alt="Imagen" width="60px"  class="bi-fondo">
            <h2 class="titulo-bi">Tienda</h2>
        </div>
    </div>

    <style>
        .bi-exp {
            position: relative;
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
        }
        .bi-fondo {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
            z-index: -1;
        }
        .titulo-bi {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            position: relative;
            z-index: 1;
            
        
        }
    </style>
    <p>
        <?= Html::a('Crear Objetos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    <style>
        .btn.btn-success {
        /* tamaño del botón */
            padding: 10px 20px; 
            font-size: 1em; 
            border: none;
            border-radius: 20px;
            background-color: #AAD399;
            color: black; 
            display: inline-block;
            cursor: pointer;
            box-shadow: 0 4px #759e64; 
            transition-duration: 0.4s;
        }

        .btn.btn-success:hover {
            background-color: #F5DF61;
            box-shadow: 0 4px #c9b649;
        }

        .btn.btn-success:active {
            background-color: #3e8e41; 
            box-shadow: 0 2px #666; 
            transform: translateY(2px); 
        }
    </style>

    
 <!--Contenedor de las monedas-->
<div class="monedero-info" style="color: #aad399; border: #aad399; top: 200px; right: 10px; background-color: transparent; padding: 10px; border-radius: 5px; ">
    
    <br>
    <!--Funcion encode para mostrar la cantidad de monedas-->
    <strong>Cantidad:</strong> <?= Html::encode($monedero->cantidad) ?> monedas
</div>  
    
    
    
    
    
    
    
    
    

<div class="objetos-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <!--BUCLE MOSTRAR OBJETOS CREADOS-->
        <?php foreach ($objetos as $objeto): ?> 
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <!--Nombre del objeto + otros datos-->
                        <h5 class="card-title"><?= Html::encode($objeto->nombre) ?></h5>
                        <p class="card-text"><?= Html::encode($objeto->descripcion) ?></p>
                        <p class="card-text">Precio: <?= Html::encode($objeto->precio) ?> monedas</p>
                        
                        <!--Formulario para comrar objetos-->
                        <?= Html::beginForm(['objetos/comprar'], 'post') ?>
                            <!--Campo oculto de la clave-->
                            <?= Html::hiddenInput('cod_items', $objeto->cod_items) ?>
                            <?= Html::submitButton('Comprar', ['class' => 'btn btn-primary']) ?>
                        <?= Html::endForm() ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
    
  <!--BUCLE MOSTRAR OBJETOS COMPRADOS-->
  <div class="objetos-comprados">
    <h2>Objetos Comprados</h2>
    <ul>
        <?php foreach ($objetosComprados as $comprado): ?>
            <li><?= Html::encode($comprado->objeto->nombre) ?></li>
        <?php endforeach; ?>
    </ul>
</div>

    
    
<!--MAQUETACION DE CARDS-->
<style>
    
    .grid-view th {
        background-color: transparent; 
        color: #DA107B; 
        border-bottom: 2px solid #aad399; 
        border-right: 2px solid #aad399; 
        border-left: 2px solid #aad399;
        border-top: 2px solid #aad399;
        color: #aad399; 
        border: 2px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5);
        
    }

    .grid-view td {
        background-color: transparent;
        color: #aad399; 
        border-bottom: 2px solid #aad399; 
        border-right: 2px solid #aad399; 
        border-left: 2px solid #aad399;
        border-top: 2px solid #aad399;
        color: #aad399; 
        border: 1px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold;
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }
    
    
    .barra-vida-container {
        width: 100%;
        background-color: #e0e0e0;
        border-radius: 4px;
        margin: 10px 0;
    }

    .barra-vida {
        height: 20px;
        background-color: #76ff03;
        border-radius: 4px;
        text-align: center;
        line-height: 20px;
        color: black;
    }

    

</style>





</div>
