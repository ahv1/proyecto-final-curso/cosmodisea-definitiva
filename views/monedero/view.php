<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\monedero $model */

$this->title = $model->cod_mon;
$this->params['breadcrumbs'][] = ['label' => 'Monederos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="monedero-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_mon' => $model->cod_mon], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_mon' => $model->cod_mon], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_mon',
            'cantidad',
        ],
    ]) ?>

</div>
