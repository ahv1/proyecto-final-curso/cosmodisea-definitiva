<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\monedero $model */

$this->title = 'Update Monedero: ' . $model->cod_mon;
$this->params['breadcrumbs'][] = ['label' => 'Monederos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_mon, 'url' => ['view', 'cod_mon' => $model->cod_mon]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monedero-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
