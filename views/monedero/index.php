<?php

use app\models\monedero;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Monederos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monedero-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Monedero', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_mon',
            'cantidad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, monedero $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cod_mon' => $model->cod_mon]);
                 }
            ],
        ],
    ]); ?>


</div>
