<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\monedero $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="monedero-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_mon')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
