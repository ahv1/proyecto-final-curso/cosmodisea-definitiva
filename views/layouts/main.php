<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;



AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    <?php

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Yii::$app->request->baseUrl . '/img/favicon.png']);
?>

</head>
<body class="d-flex flex-column h-100 fuentegv" style="background-color: black" style="color:#d6d6d6" >
<?php $this->beginBody() ?>

    
    
<header>
    
    
    
    <style>
     @keyframes brillo {
        0% { box-shadow: 0 0 5px #f5df61; }
        50% { box-shadow: 0 0 20px #f5df61; }
        100% { box-shadow: 0 0 5px #f5df61; }
    }
    .navbar {
        animation: brillo 10s infinite alternate;
        background-color: #f5df61; 
        border: 2px solid #f5df61; 
        border-radius: 15px; 
        color: #f5df61; 
        margin-top: 20px; 
        margin-left: 20px; 
        margin-right: 20px; 
        position: fixed;
        top: 0;
        width: 97%;
        z-index: 1030; 
    }

    .navbar a {
        color: #f5df61; 
    }

    .navbar a:hover,
    .navbar a:focus {
        background-color: #dbc858; 
        color: #f5df61;
    }
    </style>


    
    <?php
        
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-light', // Modificado aquí
    ],


    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            
            ['label' => 'Tripulantes', 'url' => ['/tripulantes/index']],
            ['label' => 'Misiones', 'url' => ['/misiones/index']],
            ['label' => 'Tablón', 'url' => ['/pizarra/index']],
            ['label' => 'Bitacora', 'url' => ['/bitacora/index']], 
            ['label' => 'Tienda', 'url' => ['/objetos/index']],
            ['label' => 'Bestiario', 'url' => ['/criaturas/index']],
            
            
           /**  
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )*/
        ],
    ]);
    
   
    NavBar::end();
    ?>
        
        


</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>


    
    
    
   
    
    
    
    

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


