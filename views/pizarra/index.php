<?php
use yii\helpers\Html;
/** @var yii\web\View $this */
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'My Yii Application';
?>

<?php
$this->registerCssFile('@web/css/site.css');
$this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<body>
    
    
<div class="contenedor-principal">
    

<div id="seccion-pizarra" class="seccion">
    
<div id="container" class="pizarra-container" style="width: 100%; height: 410px; position: relative;">
  <button class= "up-btn"id="upload-btn">Upload</button>
  <input type="file" id="image-upload" style="display: none;" multiple>
</div>
    
    <style>
        .up-btn {
        /* tamaño del botón */
            padding: 10px 20px; 
            font-size: 0.7em; 
            border: none;
            border-radius: 20px;
            background-color: #AAD399;
            color: black; 
            display: inline-block;
            cursor: pointer;
            box-shadow: 0 4px #759e64; 
            transition-duration: 0.4s;
        }

        .up-btn:hover {
            background-color: #F5DF61;
            box-shadow: 0 4px #c9b649;
        }

        .up-btn:active {
            background-color: #3e8e41; 
            box-shadow: 0 2px #666; 
            transform: translateY(2px); 
        }
    </style>


<?php
// PANTALLA

//bloque de JavaScript que ejecuta la accion cuando el documento esta subido
$this->registerJs("$(document).ready(function(){
    
    //asigna el evento click al boton de upload
  $('#upload-btn').click(function(){
    $('#image-upload').click();
  });

     //asigna un evento change al input tipo file 
  $('#image-upload').change(function(){
        
        //obtiene los archivos seleccionados
    var files = this.files;
    
        //itera sobre cada archivo seleccionado
    for(var i = 0; i < files.length; i++){
      var file = files[i];
      
        //crea objeto FileReader para leer el contenido del archivo
      var reader = new FileReader();
      
        //funcion que se ejecuta cuando el FileReader haya cargado el archivo
      reader.onload = function(e){
      
            //crea un elemento img con la imagen resultante
        var img = $('<img>').attr('src', e.target.result).css({
                
            //maqueta la imagen con position absolute para poder moverla
          position: 'absolute',
          width: '200px',
          height: 'auto',
          top: '0',
          left: '0',
          cursor: 'move',
          'z-index': 1000
          
            //restringe el movimiento de la imagen al contenedor 'container'
        }).draggable({
          containment: '#container', //restringe el movimiento al contenedor
          scroll: false
        });
        
            //añade la imagen al contenedor
        $('#container').append(img);
      };
      
        //lee el archivo como una url de datos base64
      reader.readAsDataURL(file);
    }
  });
});");
?>




</div>


    
    
    
    
    
    
    
    
    
    
    
<div id="seccion-barra-vida" class="seccion">   
<?php
// BARRA DE VIDA TRIPULANTES
$tripulantes = \app\models\Tripulantes::find()->all(); //obtiene los tripulantes de la bd

//registra un bloque en JavaScript
$this->registerJs("
    //asigna evento click para el botón de daño
    $('.damage-btn-tripulante').on('click', function(e) {
        e.preventDefault(); //previene el comportamiento por defecto del botón
        var idTripulante = $(this).data('id'); //obtiene el id del tripulante asociado al botón
        var vidaActual = parseInt($('#vida-' + idTripulante).text()); //saca la vida del tripulante
        var daño = 2.5; // Daño por clic para que 40 clics derroten al tripulante
        var nuevaVida = vidaActual - daño; //recalcula el total de vida después del daño

        if (nuevaVida > 100) {
            nuevaVida = 100; //asegura que la vida no exceda el 100%
        }

        //si la vida actual es mayor que el daño, actualiza la vida y la barra visual
        if (vidaActual > daño) {
            $('#vida-' + idTripulante).text(nuevaVida.toFixed(1)); //muestra solo un decimal
            $('#barra-' + idTripulante).css('width', nuevaVida + '%'); //actualiza el ancho de barra
        } else {
            $('#vida-' + idTripulante).text('0');
            $('#barra-' + idTripulante).css('width', '0%');
            alert(idTripulante + ' ha sido derrotado!'); //muestra alerta de que el tripulante ha sido derrotado
        }
    });

    //asigna evento click para el botón de cura
    $('.heal-btn-tripulante').on('click', function(e) {
        e.preventDefault(); //previene el comportamiento por defecto del botón
        var idTripulante = $(this).data('id'); //obtiene el id del tripulante asociado al botón
        var vidaActual = parseInt($('#vida-' + idTripulante).text()); //saca la vida del tripulante
        var cura = 2.5; //cantidad de vida recuperada por clic
        var nuevaVida = vidaActual + cura; //recalcula el total de vida después de la cura

        if (nuevaVida > 100) {
            nuevaVida = 100; //asegura que la vida no exceda el 100%
        }

        //actualiza la vida y la barra visual
        $('#vida-' + idTripulante).text(nuevaVida.toFixed(1)); //muestra solo un decimal
        $('#barra-' + idTripulante).css('width', nuevaVida + '%'); //actualiza el ancho de barra
    });
");
?>

<div class="site-index">
    <div class="body-content">
        <h2 style="color: #f5df61; margin-bottom: 20px;">Tripulantes</h2>
        
        <?php foreach ($tripulantes as $tripulante): ?>
            <div class="row tripulante" style="margin-bottom: 10px;">
                <div class="col-lg-2">
                    <?= Html::encode($tripulante->nombre) ?>
                </div>
                <div class="col-lg-6">
                    <div id="barra-<?= Html::encode($tripulante->n_jugador) ?>" class="barra-vida" style="background-color: #AAD399; width: <?= $tripulante->vida ?>%; height: 10px;">
                        <span id="vida-<?= Html::encode($tripulante->n_jugador) ?>"><?= $tripulante->vida ?></span>% <!--muestra el porcentaje de vida-->
                    </div>
                </div>
                <div class="col-lg-4">
                    <?= Html::button('Daño', ['class' => 'damage-btn-tripulante', 'data-id' => Html::encode($tripulante->n_jugador), 'style' => 'width: 80px; height: 40px;']) ?>
                    <?= Html::button('Cura', ['class' => 'heal-btn-tripulante', 'data-id' => Html::encode($tripulante->n_jugador), 'style' => 'width: 80px; height: 40px; margin-left: 10px;']) ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>






  

<style>

.tripulante .col-lg-2 {
    display: flex;
    align-items: center;
    font-size: 0.9em; 
    padding: 5px; 
}

.barra-vida {
    border-radius: 10px; 
    height: 12px; 
    background-color: #aad399;
    display: flex;
    align-items: center;
    position: relative;
    width: 60%; 
    margin-top:65px; 
}

.barra-vida span {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    color: white; 
    font-size: 0.8em; 
}

.damage-btn-tripulante, .heal-btn-tripulante {
    padding: 3px 6px; 
    font-size: 0.7em; 
    border: none;
    border-radius: 20px;
    background-color: #F5DF61;
    color: black;
    cursor: pointer;
    box-shadow: 0 4px #c9b649;
    transition-duration: 0.4s;
    margin: 5px; 
    width: 60px; 
}

.damage-btn-tripulante:hover{
    background-color: #c44a41;
    box-shadow: 0 4px #a6352d;
}

.damage-btn-tripulante:active {
    background-color: #3e8e41;
    box-shadow: 0 2px #666;
    transform: translateY(2px);
}

.heal-btn-tripulante:hover {
    background-color: #57a62c;
    box-shadow: 0 4px #3a6b1f;
}

.heal-btn-tripulante:active{
    background-color: #57a62c;
    box-shadow: 0 2px #3a6b1f;
    transform: translateY(2px);
}



</style>

</div>
    
    
    
    
    
    
    
<div id="seccion-barra-vida-criaturas" class="seccion">
    <?php
    // BARRA DE VIDA DE CRIATURAS
    $criaturas = \app\models\Criaturas::find()->all(); // Obtiene las criaturas de la BD

    //registra un bloque en JavaScript
    $this->registerJs("
        // Asigna evento change al desplegable
        $('#criaturas-dropdown').on('change', function() {
            var idCriatura = $(this).val(); //obtiene el id de la criatura seleccionada
            var nombreCriatura = $('#criaturas-dropdown option:selected').text(); //obtiene el nombre de la criatura seleccionada

            //verifica si la criatura ya está en la lista
            if ($('#barra-' + idCriatura).length === 0) {
                //agrega la barra de vida de la criatura seleccionada
                $('#seccion-barra-vida-criaturas').append(`
                    <div class='row criatura' style='margin-bottom: 10px;'>
                        <div class='col-lg-2'>` + nombreCriatura + `</div>
                        <div class='col-lg-6'>
                            <div id='barra-` + idCriatura + `' class='barra-vida-cri' style='background-color: #AAD399; width: 75%; height: 10px; margin-top: 50px;'>
                                <span id='vida-` + idCriatura + `'>100</span>%
                            </div>
                        </div>
                        <div class='col-lg-3'>
                            <button class='damage-btn-criatura' data-id='` + idCriatura + `' style='width: 80px; height: 40px; margin-right: 60px;'>Daño</button>
                            <button class='heal-btn-criatura' data-id='` + idCriatura + `' style='width: 80px; height: 40px;'>Cura</button>
                        </div>
                    </div>
                `);
            }
        });

        //asigna evento click a los botones de daño
        $(document).on('click', '.damage-btn-criatura', function(e) {
            e.preventDefault(); //previene el comportamiento por defecto del botón
            var idCriatura = $(this).data('id'); //obtiene el id de la criatura asociado al botón
            var vidaActual = parseFloat($('#vida-' + idCriatura).text()); //saca la vida de la criatura
            var daño = 1.5;  //cantidad de daño por click
            var nuevaVida = vidaActual - daño; //recalcula el total de vida después del daño

            if (nuevaVida > 100) {
                nuevaVida = 100; //asegura que la vida no exceda el 100%
            }

            //si la vida actual es mayor que el daño, actualiza la vida y la barra visual
            if (vidaActual > daño) {
                $('#vida-' + idCriatura).text(nuevaVida.toFixed(1)); //muestra solo un decimal
                $('#barra-' + idCriatura).css('width', nuevaVida + '%'); //actualiza el ancho de la barra
            } else {
                $('#vida-' + idCriatura).text('0');
                $('#barra-' + idCriatura).css('width', '0%');
                alert(idCriatura + ' ha sido derrotado!'); //muestra alerta de que la criatura ha sido derrotada
            }
        });

        //asigna evento click a los botones de cura
        $(document).on('click', '.heal-btn-criatura', function(e) {
            e.preventDefault(); //previene el comportamiento por defecto del botón
            var idCriatura = $(this).data('id'); //obtiene el id de la criatura asociado al botón
            var vidaActual = parseFloat($('#vida-' + idCriatura).text()); //saca la vida de la criatura
            var cura = 1.5;  //cantidad de cura por click
            var nuevaVida = vidaActual + cura; //recalcula el total de vida después de la cura

            if (nuevaVida > 100) {
                nuevaVida = 100; //asegura que la vida no exceda el 100%
            }

            //si la vida actual es menor que 100, actualiza la vida y la barra visual
            if (vidaActual < 100) {
                $('#vida-' + idCriatura).text(nuevaVida.toFixed(1)); //muestra solo un decimal
                $('#barra-' + idCriatura).css('width', nuevaVida + '%'); //actualiza el ancho de la barra
            }
        });
    ");
    ?>

    <div class="site-index">
        <div class="body-content">
            <div class="form-group">
                <label for="criaturas-dropdown">Selecciona una criatura:</label>
                <select id="criaturas-dropdown" class="form-control">
                    <option value="">-- Selecciona una criatura --</option>
                    <?php foreach ($criaturas as $criatura): ?>
                        <option value="<?= Html::encode($criatura->cod_cria) ?>"><?= Html::encode($criatura->nom_criaturas) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
</div>




    

<style>
.criatura {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 10px;
    padding: 10px;
    border: none; 
    
}

.nombre-criatura {
    flex: 1;
    text-align: left;
    font-size: 0.8em; 
    margin-right: 10px;
}

.barra-vida-cri-container {
    flex: 2; 
    display: flex;
    align-items: center;
}

.barra-vida-cri {
    flex-grow: 1;
    height: 10px;
    background-color: #aad399;
    border-radius: 5px;
    position: relative;
     
}

.barra-vida-cri span {
    position: absolute;
    left: 50%;
    top: 70%;
    transform: translate(-50%, -50%);
    color: white;
    font-size: 0.7em; 
}

.damage-btn-criatura, .heal-btn-criatura {
    padding: 3px 6px;
    font-size: 0.7em;
    border: none;
    border-radius: 30px;
    cursor: pointer;
    transition-duration: 0.4s;
    margin: 5px;
    width: 60px;
}

.damage-btn-criatura {
    background-color: #f5df61;
    color: black;
    box-shadow: 0 5px #c9b649;
}

.damage-btn-criatura:hover {
    background-color: #c44a41;
    box-shadow: 0 4px #a6352d;
}

.damage-btn-criatura:active {
    background-color: #3e8e41;
    box-shadow: 0 2px #666;
    transform: translateY(2px);
}

.heal-btn-criatura {
    background-color: #f5df61;
    color: black;
    box-shadow: 0 5px #c9b649;
}

.heal-btn-criatura:hover {
    background-color: #57a62c;
    box-shadow: 0 4px #3a6b1f;
}

.heal-btn-criatura:active {
    background-color: #57a62c;
    box-shadow: 0 2px #3a6b1f;
    transform: translateY(2px);
}


</style>




</style>

</div>


    
    
    
    
    
    
    
    
    


<!--SECCION BUCLE DE OBJETOS COMPRADOS-->

<div id="seccion-objetos" class="seccion">
    <h2 class="titulo-objetos">Objetos</h2>
    
    <?php foreach ($objetosComprados as $comprado): ?>
        <li>
            <?= Html::encode($comprado->objeto->nombre) ?>
            <?= Html::beginForm(['pizarra/usar', 'id' => $comprado->id], 'post') ?>
                <?= Html::submitButton('Usar', ['class' => 'btn btn-success']) ?>
            <?= Html::endForm() ?>
        </li>
    <?php endforeach; ?>
</div>














<div id="dados" class="seccion-dados">

<?php

// BOTON DE DADOS
$this->registerJs("
$('.dice-btn').on('click', function(e) {
    e.preventDefault(); //previene la acción por defecto del enlace
    $.ajax({
        url: '" . yii\helpers\Url::to(['dice/lanzar-dado']) . "', //URL para la solicitud AJAX
        type: 'get', //tipo de solicitud
        success: function(data) {
            $('.resultado-dado').text(data.resultado); //actualiza el texto del contenedor con el resultado del dado
        }
    });
});
");
?>

<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-6" style="position: relative;">
                <!-- Contenedor para mostrar el resultado del dado -->
                <div class="resultado-dado" style="position: absolute; top: 55%; left: 90%; transform: translate(-50%, -50%); font-size: 17px; font-weight: bold; color: black; z-index: 10;">
                    
                </div>
                <!-- Imagen del dado con un enlace que activa el evento de clic -->
                <?= Html::a(Html::img('@web/img/dado.png', ['style' => 'width: 90px; height: 90px; position: relative; z-index: 1;']), '#', ['class' => 'dice-btn']) ?>
            </div>
        </div>
    </div>
</div>

</div>






    
    
    
    
    
    
    
    


<div id="seccion-botones-sonido" class="seccion" style="background-image: url('web/img/ICONS/MUSICA.png');">
    
  <!--EFECTOS SONIDO-->
  <style>
    .icon-music {
        display: flex;
        align-items: center; 
    }

    .left {
        margin-right: 20px; 
    }

    .btn-audio {
        
        padding: 5px 10px; 
        font-size: 0.7em; 
        border: none;
        border-radius: 20px;
        background-color: #AAD399;
        color: black; 
        display: inline-block;
        cursor: pointer;
        box-shadow: 0 4px #759e64; 
        transition-duration: 0.4s;
    }

    .btn-audio:hover {
        background-color: #F5DF61;
        box-shadow: 0 4px #c9b649;
    }

    .btn-audio:active {
        background-color: #3e8e41; 
        box-shadow: 0 2px #666; 
        transform: translateY(2px); 
    }
    
    .pause{
        margin-left: 20px;
    }
</style>

<div class="icon-music">
    <img src="<?= Yii::getAlias('@web') ?>/img/ICONS/MUSICA.png" alt="Imagen" width="60px"  class="left">
    <div>
        <?= Html::button('Combate', ['class' => 'btn-audio', 'onclick' => 'playAudio("combate")']) ?>
        <?= Html::button('Ambiente', ['class' => 'btn-audio', 'onclick' => 'playAudio("ambiente")']) ?>
        <?= Html::button('Alarma', ['class' => 'btn-audio', 'onclick' => 'playAudio("alarma")']) ?>
        <?= Html::button('Espada', ['class' => 'btn-audio', 'onclick' => 'playAudio("espada")']) ?>
        <?= Html::button('Explosión', ['class' => 'btn-audio', 'onclick' => 'playAudio("explosion")']) ?>
        <?= Html::button('Pistola', ['class' => 'btn-audio', 'onclick' => 'playAudio("pistola")']) ?>
        <?= Html::button('Rugido', ['class' => 'btn-audio', 'onclick' => 'playAudio("roar")']) ?>
        <?= Html::button('<img src="' . Yii::getAlias('@web') . '/img/ICONS/STOP.png" alt="Imagen" width="50px" class="pause">', ['onclick' => 'stopAllAudios()','style' => 'background: transparent; border: none; padding: 0;'
]) ?><!-- BOTÓN PARA DETENER TODOS LOS AUDIOS -->

        
    </div>
</div>

  
 

<!-- REPRODUCTORES OCULTOS -->

<audio id="audio-combate" src="<?= Yii::$app->request->baseUrl ?>/mp3/combate1.mp3" hidden></audio>
<audio id="audio-ambiente" src="<?= Yii::$app->request->baseUrl ?>/mp3/ambiente1.mp3" hidden></audio>
<audio id="audio-alarma" src="<?= Yii::$app->request->baseUrl ?>/mp3/alarma.mp3" hidden></audio>
<audio id="audio-espada" src="<?= Yii::$app->request->baseUrl ?>/mp3/espada.mp3" hidden></audio>
<audio id="audio-explosion" src="<?= Yii::$app->request->baseUrl ?>/mp3/explosion.mp3" hidden></audio>
<audio id="audio-pistola" src="<?= Yii::$app->request->baseUrl ?>/mp3/pistola.mp3" hidden></audio>
<audio id="audio-roar" src="<?= Yii::$app->request->baseUrl ?>/mp3/roar.mp3" hidden></audio>

<script>
  // FUNCION PLAY
  function playAudio(audioName) {
    if (audioName === "ambiente1" || audioName === "combate1") {
      stopAllAudios();
    }
    var audio = document.getElementById("audio-" + audioName);
    if (!audio.paused || audio.currentTime) {
      return; //si ya se está reproduciendo, no hacer nada
    }
    audio.play();
  }

  // FUNCION STOP
  function stopAllAudios() {
    var audios = ["audio-combate", "audio-ambiente", "audio-alarma", "audio-espada", "audio-explosion", "audio-pistola", "audio-roar"];
    audios.forEach(function(audioID) {
      var audio = document.getElementById(audioID);
      audio.pause();
      audio.currentTime = 0;
    });
  }
</script>

</div>
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
 <style>
    .contenedor-principal {
        display: grid;
        gap: 10px;
        height: 100vh;
        padding: 10px;
    }

    .seccion {
        background-color: #000; 
        color: #aad399; 
        border: 1px solid #aad399; 
        box-shadow: 0 0 20px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px;
        
        
        transition: transform 0.5s ease, box-shadow 0.5s ease; /*transición suave para hover */
    }
    
    .seccion-dados {
        position: absolute; 
        top: 580px;
        left: 895px;
        width: 100px; 
        height: 100px; 
    }

    
    
    @keyframes pulsate {
        0% { box-shadow: 0 0 5px #30cfd0; }
        50% { box-shadow: 0 0 20px #30cfd0; }
        100% { box-shadow: 0 0 5px #30cfd0; }
    }
    
    .seccion:hover {
        animation: pulsate 2s infinite;
    }

    #seccion-pizarra {
        position: absolute; 
        top: 125px; 
        left: 330px; 
        width: 670px; 
        height: 440px; 
        overflow: hidden; 
    }

    #seccion-barra-vida {
        position: absolute; 
        width: 100%; 
        max-width: 400px; 
        max-height: 260px; 
        overflow: hidden; 
        max-height: 260px; 
        overflow-y: auto;
        position: absolute; 
        top: 125px; 
        left: 1020px; 
        height: 260px; 
        
        overflow-y: auto;
    }
    
    
    #seccion-barra-vida::-webkit-scrollbar {
        width: 10px; 
    }

    #seccion-barra-vida::-webkit-scrollbar-track {
        background: rgba(255, 255, 255, 0.1); 
        border-radius: 10px; 
    }

    #seccion-barra-vida::-webkit-scrollbar-thumb {
        background: linear-gradient(45deg, #aad399, #4e8a36); 
        border-radius: 10px; 
    }

    #seccion-barra-vida::-webkit-scrollbar-thumb:hover {
        background: linear-gradient(45deg, #f5df61, #f5ba61, #f5df61); 
    }
    
    #seccion-barra-vida-criaturas {
        position: absolute; 
        width: 100%; 
        max-width: 400px; 
        max-height: 260px; 
        overflow: hidden; 
        max-height: 260px; 
        overflow-y: auto;
        position: absolute; 
        top: 410px; 
        left: 1020px; 
        height: 260px; 
        
        overflow-y: auto;
    
    }

    #seccion-barra-vida-criaturas::-webkit-scrollbar {
        width: 10px; 
    }

    #seccion-barra-vida-criaturas::-webkit-scrollbar-track {
        background: rgba(255, 255, 255, 0.1); 
        border-radius: 10px; 
    }

    #seccion-barra-vida-criaturas::-webkit-scrollbar-thumb {
        background: linear-gradient(45deg, #aad399, #4e8a36);
        border-radius: 10px;
    }

    #seccion-barra-vida-criaturas::-webkit-scrollbar-thumb:hover {
        background: linear-gradient(45deg, #f5df61, #f5ba61, #f5df61); 
    }
    
    
    
    

    #seccion-objetos {
        position: absolute; 
        top: 125px; 
        left: 100px; 
        width: 200px; 
        height: 440px; 
        overflow: hidden;
        overflow-y: auto;
    }

    #seccion-objetos::-webkit-scrollbar {
        width: 10px; 
    }

    #seccion-objetos:-webkit-scrollbar-track {
        background: rgba(255, 255, 255, 0.1); 
        border-radius: 10px; 
    }

    #seccion-objetos::-webkit-scrollbar-thumb {
        background: linear-gradient(45deg, #aad399, #4e8a36); 
        border-radius: 10px; 
    }

    #seccion-objetos::-webkit-scrollbar-thumb:hover {
        background: linear-gradient(45deg, #f5df61, #f5ba61, #f5df61); 
    }
    
    .titulo-objetos {
        color: #F5DF61;
        font-size: 18px;
        margin-bottom: 10px;
    }

    

    #seccion-botones-sonido {
        position: absolute;
        top: 590px; 
        left: 100px;
        width: 750px; 
        height: 80px; 
    }
    
    
    .btn-mascota {
        background-color: transparent; 
        color: white; 
        border: 2px solid #aad399; 
        font-size: 16px; 
        border-radius: 15px; 
        position: fixed;
        bottom: 50px;
        right: 30px;
        z-index: 100;
        padding: 10px 20px; 
        text-transform: uppercase;
        font-weight: bold;
        cursor: pointer;
        transition: transform 0.2s ease-in-out;
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5);     
    }


    .btn-fijo-site:hover {
        transform: scale(1.1);
    }
    
</style>
   
    




<!-- BOTON MASCOTA -->
<?= Html::a('?', [''], ['class' => 'btn-mascota', 'id' => 'boton-toggle']) ?>

<!-- CONTENEDOR MASCOTA -->
<div id="imagen-container" style="display: none; position: fixed; bottom: 100px; right: 30px; z-index: 101; background: transparent;">
    <img id="imagen-mascota" class="d-block w-1" src="<?= Yii::getAlias('@web')?>/img/ATH/ATH-PI-OBJ.png" style="width: 500px; height: auto;">
    <!-- BOTON SIGUIENTE -->
    <button id="boton-siguiente" class="btn btn-secondary">Siguiente</button>
</div>

<!-- JS MANEJAR MASCOTA -->
<script>
    document.getElementById('boton-toggle').addEventListener('click', function(event) {
        event.preventDefault();

        var boton = this;
        var imagenContainer = document.getElementById('imagen-container');

        if (imagenContainer.style.display === 'none') {
            imagenContainer.style.display = 'block';
            boton.innerHTML = 'X';
        } else {
            imagenContainer.style.display = 'none';
            boton.innerHTML = '?';
        }
    });

    //array de imágenes
    var imagenes = [
        "<?= Yii::getAlias('@web')?>/img/ATH/ATH-PI-OBJ.png",
        "<?= Yii::getAlias('@web')?>/img/ATH/ATH-PI-MUS.png",
        "<?= Yii::getAlias('@web')?>/img/ATH/ATH-PI-PI.png",
        "<?= Yii::getAlias('@web')?>/img/ATH/ATH-PI-DA.png"
    ];
    var indiceImagenActual = 0;

    función para cambiar la imagen
    function cambiarImagen() {
        indiceImagenActual = (indiceImagenActual + 1) % imagenes.length;
        document.getElementById('imagen-mascota').src = imagenes[indiceImagenActual];
    }

    //evento para el botón Siguiente
    document.getElementById('boton-siguiente').addEventListener('click', cambiarImagen);
</script>





    
    
    


</div>
</body>