<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\misiones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="misiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nom_mis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'informe')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'dificultad')->dropDownList(
        [1 => '1', 2 => '2', 3 => '3'], // Opciones del desplegable
        ['prompt' => 'Seleccione la dificultad'] // Texto del placeholder
    ) ?>
    
    
    <!-- BOTON MASCOTA INSTRUCCIONES PARA LA DIFICULTAD -->
    <?= Html::a('?', [''], ['class' => 'btn btn-primary btn-fijo', 'id' => 'boton-toggle']) ?>



    <!-- CONTENEDOR MASCOTA -->
    <div id="imagen-container" style="display: none; position: fixed; bottom: 100px; right: 30px; z-index: 101;">
        <img class="d-block w-1" src="<?= Yii::getAlias('@web')?>/img/prueba.png" style="width: 500px; height: auto;">
    </div>




    <!-- JS MANEJAR MASCOTA -->
    <script>
    document.getElementById('boton-toggle').addEventListener('click', function(event) {
        // Previene el comportamiento por defecto del enlace
        event.preventDefault();

        var boton = this;
        var imagenContainer = document.getElementById('imagen-container');

        // Verifica si la imagen ya está visible
        if (imagenContainer.style.display === 'none') {
            // Muestra la imagen
            imagenContainer.style.display = 'block';

            // Cambia la apariencia del botón a 'X'
            boton.innerHTML = 'X';
        } else {
            // Oculta la imagen
            imagenContainer.style.display = 'none';

            // Restablece la apariencia del botón a '?'
            boton.innerHTML = '?';
        }
    });
    </script>

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    <?= $form->field($model, 'pago')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
