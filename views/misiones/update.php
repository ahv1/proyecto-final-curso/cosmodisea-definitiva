<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\misiones $model */

$this->title = 'Update Misiones: ' . $model->nom_mis;
$this->params['breadcrumbs'][] = ['label' => 'Misiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nom_mis, 'url' => ['view', 'nom_mis' => $model->nom_mis]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="misiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
