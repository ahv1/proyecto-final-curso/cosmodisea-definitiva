
<style>
    
.mision-container {
    background-color: transparent;
    color: #aad399;
    border: 1px solid #aad399;
    box-shadow: 0 0 20px #aad399;
    padding: 10px 20px;
    font-size: 16px;
    font-weight: bold;
    border-radius: 15px;
    word-wrap: break-word;
    margin-bottom: 20px;
    
    transition: transform 0.5s ease, box-shadow 0.5s ease;
}

.mision-title {
    color: #f5df61;
    font-size: 24px;
}

.mision-actions a {
    color: #f5df61;
    text-decoration: none;
    margin-right: 10px;
}

@keyframes pulsate {
    0% { box-shadow: 0 0 5px #30cfd0; }
    50% { box-shadow: 0 0 20px #30cfd0; }
    100% { box-shadow: 0 0 5px #30cfd0; }
}
    
.mision-container:hover {
    animation: pulsate 2s infinite;
}
</style>


<?php
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;

//suponiendo que $misionesNivel1 es un array de objetos de misiones
$dataProvider = new ArrayDataProvider([
    'allModels' => $misionesNivel2,
    'pagination' => [
        'pageSize' => 10, //cantidad de misiones por página
    ],
]);

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model, $key, $index, $widget) {
        //como se muestra cada mision
        $content = "<div class=\"mision-container\">";
        $content .= "<p class=\"mision-title\"> " . $model->nom_mis . "</p>";
        $content .= "<p>Dificultad: " . $model->dificultad . "</p>";
        $content .= "<p>Informe: " . $model->informe . "</p>";
        $content .= "<p>Pago: " . $model->pago . "</p>"; 
        
        //botón de actualizar
        $updateUrl = Yii::$app->urlManager->createUrl(['misiones/update', 'nom_mis' => $model->nom_mis]);
        $content .= '<a href="' . $updateUrl . '" class="btn btn-success" style="background: transparent; border: none; padding: 0;">
                <img src="' . Yii::getAlias('@web') . '/img/ICONS/update.png" alt="Actualizar" width="50px">
            </a>';

        //botón de eliminar
        $deleteUrl = Yii::$app->urlManager->createUrl(['misiones/delete', 'nom_mis' => $model->nom_mis]);
        $content .= '<a href="' . $deleteUrl . '" class="btn btn-danger" data-method="post" data-confirm="¿Estás seguro de que quieres eliminar esta misión?" style="background: transparent; border: none; padding: 0;">
                <img src="' . Yii::getAlias('@web') . '/img/ICONS/borrar.png" alt="Eliminar" width="50px">
            </a>';

        //formulario para completar la misión
        $content .= "<div class=\"mision-actions\">";
        $content .= "<form action=\"" . Yii::$app->urlManager->createUrl(['misiones/completar', 'nom_mis' => $model->nom_mis]) . "\" method=\"post\">";
        $content .= "<input type=\"hidden\" name=\"" . Yii::$app->request->csrfParam . "\" value=\"" . Yii::$app->request->csrfToken . "\">";
        $content .= "<button type=\"submit\" class=\"btn btn-primary\">Completar</button>";
        $content .= "</form>";
        $content .= "</div>";
        $content .= "</div>";

        return $content;
    },
    'layout' => "{summary}\n{items}\n{pager}", 
]);
