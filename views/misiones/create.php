<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\misiones $model */

$this->title = 'Crear Misiones';
/**$this->params['breadcrumbs'][] = ['label' => 'Misiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="misiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
