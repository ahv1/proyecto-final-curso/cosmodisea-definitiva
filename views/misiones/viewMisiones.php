<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $misiones app\models\Mision[] */

$this->title = 'Misiones';
$this->params['breadcrumbs'][] = ['label' => 'Planetas', 'url' => ['planetas/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="misiones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($misiones as $mision): ?>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?= Html::encode($mision->nom_mis) ?></h5>
                <p class="card-text"><?= Html::encode($mision->informe) ?></p>
                <!-- Aquí puedes mostrar más detalles de la misión si es necesario -->
            </div>
        </div>
    <?php endforeach; ?>

</div>
