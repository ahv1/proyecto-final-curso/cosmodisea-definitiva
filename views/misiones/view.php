<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\misiones $model */

$this->title = $model->nom_mis;
$this->params['breadcrumbs'][] = ['label' => 'Misiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="misiones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'nom_mis' => $model->nom_mis], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'nom_mis' => $model->nom_mis], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nom_mis',
            'informe:ntext',
            'dificultad',
            'recompensa',
        ],
    ]) ?>

</div>
