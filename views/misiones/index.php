<?php

use app\models\misiones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider 

$this->title = 'Misiones';
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="misiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <div class="container mt-5">
        <div class="mis-exp">
            <img src="<?= Yii::getAlias('@web') ?>/img/Slider-pla.jpg" alt="Imagen" width="60px"  class="mis-fondo">
            <h2 class="titulo-mis">Misiones</h2>
        </div>
    </div>

    <style>
        .mis-exp {
            position: relative;
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
        }
        .mis-fondo {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
            z-index: -1;
        }
        .titulo-mis {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            position: relative;
            z-index: 1;
        }
    </style>
    <p>
        <?= Html::a('Crear Misiones', ['create'], ['class' => 'btn btn-success']) ?>

    </p>
    
    <style>
        .btn.btn-success {
        /* tamaño del botón */
            padding: 10px 20px; 
            font-size: 1em; 
            border: none;
            border-radius: 20px;
            background-color: #AAD399;
            color: black; 
            display: inline-block;
            cursor: pointer;
            box-shadow: 0 4px #759e64; 
            transition-duration: 0.4s;
        }

        .btn.btn-success:hover {
            background-color: #F5DF61;
            box-shadow: 0 4px #c9b649;
        }

        .btn.btn-success:active {
            background-color: #3e8e41; 
            box-shadow: 0 2px #666; 
            transform: translateY(2px); 
        }
    </style>

    
    
        
    </div>


    <h1><?= Html::encode($this->title) ?></h1>  
    
    
    
    
    
    
    <section class="pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h3 class="mb-3">Planetas </h3>
            </div>
            
            
                
            </div>
            
            
            <div class="col-12">
                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                        
                        <!-- SET 1 -->
                        <div class="carousel-item active">
                            <div class="row">

                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="<?= Yii::getAlias('@web')?>/img/DESTINOS/venus.png">
                                        <div class="card-body">
                                            <h4 class="card-title">Venus</h4>
                                            <p class="card-text"> </p>
                                            
                                            
                                            <!--BOTON NIVEL -5-->
                                            <!--Consulta si existe al menos un tripulante con un nivel mayor o igual a 1 en la base de datos-->
                                                <?php
        
        
                                                    $habilitarBoton = $tripulantes->andWhere(['>=', 'nivel', 1])->exists(); // COMPRUEBO SI NIVEL>= 10

                                                    if ($habilitarBoton) {
                                                        echo Html::a('Ver Misiones Nivel 1', ['misiones/mostrar'], ['class' => 'btn btn-success']);
                                                    } else {
                                                        echo Html::button('Ver Misiones Nivel 1', ['class' => 'btn btn-success', 'disabled' => true]);
                                                    }


                                                ?> 

                                            

                                        </div>

                                    </div>
                                </div>
                                
                                
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="<?= Yii::getAlias('@web')?>/img/DESTINOS/marte.png">
                                        <div class="card-body">
                                            <h4 class="card-title">Marte</h4>
                                            <p class="card-text"> </p>
                                            
                                            
                                            <!--BOTON NIVEL >=5-->
                                            <!--Consulta si existe al menos un tripulante con un nivel mayor o igual a 5 en la base de datos-->
                                                <?php


                                                    $habilitarBoton = $tripulantes->andWhere(['>=', 'nivel', 5])->exists(); // COMPRUEBO SI NIVEL>= 5

                                                    if ($habilitarBoton) {
                                                        echo Html::a('Ver Misiones Nivel 2', ['misiones/mostrar1'], ['class' => 'btn btn-success']);
                                                    } else {
                                                        echo Html::button('Ver Misiones Nivel 2', ['class' => 'btn btn-success', 'disabled' => true]);
                                                    }


                                                ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <img class="img-fluid" alt="100%x280" src="<?= Yii::getAlias('@web')?>/img/DESTINOS/orcus.png">
                                        <div class="card-body">
                                            <h4 class="card-title">Orcus</h4>
                                            <p class="card-text"> </p>
                                            
                                            
                                            <!--BOTON NIVEL >=10-->
                                            <!--Consulta si existe al menos un tripulante con un nivel mayor o igual a 10 en la base de datos-->
                                                 <?php


                                                    $habilitarBoton = $tripulantes->andWhere(['>=', 'nivel', 10])->exists(); // COMPRUEBO SI NIVEL>= 10

                                                    if ($habilitarBoton) {
                                                        echo Html::a('Ver Misiones Nivel 3', ['misiones/mostrar2'], ['class' => 'btn btn-success']);
                                                    } else {
                                                        echo Html::button('Ver Misiones Nivel 3', ['class' => 'btn btn-success', 'disabled' => true]);
                                                    }


                                                ?>
    
                                            
                                            

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <!-- SET 2 -->
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    

    

    





    
    
    
    
    
    
    
    


    


    
    
    
    
    
 <!-- BOTON MASCOTA -->
<?= Html::a('?', [''], ['class' => 'btn btn-primary btn-fijo', 'id' => 'boton-toggle']) ?>

<!-- CONTENEDOR MASCOTA -->
<div id="imagen-container" style="display: none; position: fixed; bottom: 100px; right: 30px; z-index: 101; background: transparent;">
    <img id="imagen-mascota" class="d-block w-1" src="<?= Yii::getAlias('@web')?>/img/ATH/ATH_MIS.png" style="width: 500px; height: auto;">
    <!-- BOTON SIGUIENTE -->
    <button id="boton-siguiente" class="btn btn-secondary">Siguiente</button>
</div>

<!-- JS MANEJAR MASCOTA -->
<script>
document.getElementById('boton-toggle').addEventListener('click', function(event) {
    event.preventDefault();
    
    var boton = this;
    var imagenContainer = document.getElementById('imagen-container');
    
    if (imagenContainer.style.display === 'none') {
        imagenContainer.style.display = 'block';
        boton.innerHTML = 'X';
    } else {
        imagenContainer.style.display = 'none';
        boton.innerHTML = '?';
    }
});

// Array de imágenes
var imagenes = [
    "<?= Yii::getAlias('@web')?>/img/ATH/ATH_MIS.png",
    
    "<?= Yii::getAlias('@web')?>/img/ANA_MISIONES_3.png"
];
var indiceImagenActual = 0;

// Función para cambiar la imagen
function cambiarImagen() {
    indiceImagenActual = (indiceImagenActual + 1) % imagenes.length;
    document.getElementById('imagen-mascota').src = imagenes[indiceImagenActual];
}

// Evento para el botón Siguiente
document.getElementById('boton-siguiente').addEventListener('click', cambiarImagen);
</script>









    
    
    
    
    
</div>
