<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $misiones Misiones[] */

$this->title = 'Completar Misiones';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php foreach ($misiones as $mision): ?>
    <div>
        <p><?= Html::encode($mision->nom_mision) ?></p>
        <p><?= Html::encode($mision->recompensa) ?></p>
        <?= Html::a('Completar', ['completar', 'nom_mision' => $mision->nom_mision], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres completar esta misión?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
<?php endforeach; ?>
