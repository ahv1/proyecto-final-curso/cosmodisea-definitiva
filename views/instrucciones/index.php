<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent tituloHome">
        <h1 class="display-4"style="color:">Tu nueva aventura te espera</h1>
        
    </div>

    
    
    <div class="body-content">
        
    <div class="body">
        
        <!-- CARRUSEL -->
    
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                
               
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
        
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Slider 1.jpg" >
                <div class="carousel-caption d-none d-md-block" style="color:#4f2e0d">
                    <h3>Haz de la nave tu fortaleza</h3>
                            <p1>Administra tu porpia nave y raciona recursos y armamento.</p1>
                </div>
                
            </div>
            
            <div class="carousel-item">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Slider 2.jpg" >
                <div class="carousel-caption d-none d-md-block" style="color:#E1BEEF">
                    <h3>El mundo a tu alcance</h3>
                            <p>Visita planetas desconocidos y haz todas las misiones que puedas.</p>
                </div>
               
            </div>
            
            <div class="carousel-item">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Slider 3.jpg" >
                <div class="carousel-caption d-none d-md-block" style="color:#E1F5FE">
                    <h3>Recluta a los mejores</h3>
                            <p1>Podras reclutar y personalizar tu propia tripulación para disponer de los mejores tripulantes.</p1>
                </div>
            </div>
        </div>
        
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        
    </div>
    
    <br>
    
    <!-- FIN CARRUSEL -->
        
        
    
    
    </div>
    </div>
    