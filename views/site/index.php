<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <!-- TITULO -->
    <div class="jumbotron text-center bg-transparent tituloHome">
        <h1 class="display-4"style="color:#F5DF61" >Tu nueva aventura te espera</h1>
        
    </div>
    
    <style>
        .display-4{
            text-shadow: 0 0 5px #AAD399;
        }
    </style>

    
    
    <div class="body-content">
        
    <div class="body">
        
        
        
        
        
        <!-- INTRO INICIAL-->
      
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            
            <style>
                .intro {
                    border: 3px solid #AAD399;
                    padding: 20px;
                    color: #AAD399;
                    box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
                    text-shadow: 0 0 5px #AAD399;
                    margin-bottom: 70px;
                    background: url('img/Intro.jpg') no-repeat center center;
                    background-size: cover;
                    position: relative;
                }
            </style>
        </head>
        <body>
            <div class="container mt-5">
                <div class="intro">
                    <p>Cosmodisea es la app ideal para dungeon masters como tu. desde el centro de los confines de la galaxia venimos a traerte las mejores herramientas para que tanto tu como tus amigos, tengais la mejor experiencia rolera que podriais desear.</p> 
                    <p>A continuacion, te explicamos lo necesario para entender esta version simplificada del clasico juego de rol. ¡Esperamos que lo paseis genial abordo de esta nave!
                </div>
            </div>
        </body>
        
        
        
        
        
        
        
        
        
    <!--TRIPULANTES-->    
        
        <h2 class="titulo-tripulantes">Tus tripulantes</h2>
        

        
        <div class="container mt-5">
                <div class="tri-exp">
                    <p>Los tripulantes se tratan de los jugadores que participaran en tu partida, Podras registrar todos tus datos.</p>
                    <p>¡Tambien podras asignarles las clases con las que tendran que rolear! Cada personaje puede tener mas de una clase, pero deben tener en cuenta que solo pueden elegir una caracteristica por clase añadida.</p>
                </div>
            
        <style>    
            .tri-exp{
                border: 3px solid #AAD399;
                padding: 20px;
                color: #AAD399;
                box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
                text-shadow: 0 0 5px #AAD399;
                margin-bottom: 50px;
                background: url('img/Slider-tri.jpg') no-repeat center center;
                background-size: cover;
                position: relative;
            }
            .titulo-tripulantes {
                color: #F5DF61;
                text-shadow: 0 0 10px #F5DF61;
                margin-bottom: 20px;
                margin-top:70px;
            }
        </style>
        
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card card-jugador mb-4">
                    <div class="card-body">
                        <h5 class="card-title" color="#F5DF61">El jugador</h5>
                        <p class="card-text">¡Pon el nombre real de tu amigo!</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-personaje mb-4">
                    <div class="card-body">
                        <h5 class="card-title" color="#F5DF61">Su personaje</h5>
                        <p class="card-text">El nombre del personaje al que os referireis en rol.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-origen mb-4">
                    <div class="card-body">
                        <h5 class="card-title" color="#F5DF61">Su origen</h5>
                        <p class="card-text">Todo buen personaje tiene una buena historia ¡Escríbela!</p>
                    </div>
                </div>
            </div>
        </div>

        
        <style>
            .card-jugador {
                background-image: url('img/jugador.jpg');
            }

            .card-personaje {
                background-image: url('img/pj3.jpg');
            }

            .card-origen {
                background-image: url('img/og.jpg');
            }
        </style>
        
  
     <!-- CARRUSEL DE CLASES -->
    
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                
               
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
        
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Mec1.jpg" >
                <div class="carousel-caption d-none d-md-block text-center" style="color:#AAD399; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
                    <h3 style="color: #F5DF61; text-shadow: 0 0 10px #F5DF61;">Mecanico</h3>
                    <h4>Toda nave necesita un mecánico:</h4>
                    <p>+5 en habilidades técnicas</p>
                    <p>+2 en inteligencia</p>
                </div>
            </div>

            
            <div class="carousel-item">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/MERC1.jpg" >
                <div class="carousel-caption d-none d-md-block text-center" style="color:#AAD399; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
                    <h3 style="color: #F5DF61; text-shadow: 0 0 10px #F5DF61;">Mercenario</h3>
                    <h4>Siempre el mas duro en las peleas:</h4>
                    <p>+1 en todos los golpes</p>
                    <p>+2 en intimidación</p>
                </div>
               
            </div>
            
            <div class="carousel-item">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/RAN1.jpg" >
                <div class="carousel-caption d-none d-md-block text-center" style="color:#AAD399; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
                    <h3 style="color: #F5DF61; text-shadow: 0 0 10px #F5DF61;">Ranger</h3>
                    <h4>No hay sitio en este planeta para dos como tu:</h4>
                    <p>+5 en rastreo</p>
                    <p>+2 turnos de invisibilidad por ataque</p>
                </div>
            </div>
            
            <div class="carousel-item">
                <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/SAN1.jpg" >
                <div class="carousel-caption d-none d-md-block text-center" style="color:#AAD399; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
                    <h3 style="color: #F5DF61; text-shadow: 0 0 10px #F5DF61;">Sanador</h3>
                    <h4>Si es que no pueden vivir sin tí:</h4>
                    <p>+5 en carisma</p>
                    <p>+2 sanación por turno de combate</p>
                </div>
            </div>
        </div>
        
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        
    </div>
    
    <br>
    
    <!-- FIN CARRUSEL -->     
    
    
    
    
    
    
    <!-- MISIONES -->  
    
    <h2 class="titulo-misiones">Tus misiones</h2>

    <style>
        .titulo-misiones {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            margin-bottom: 20px;
            margin-top:70px;
    }
    </style>
    
    <div class="container mt-5">
        <div class="mis-exp">
            Durante vuestra aventura tendréis tres planetas para explorar separados por niveles. Sube a tus jugadores de nivel para poder visitarlos.
        </div>
    </div> 
    
    

    <style>
        .mis-exp{
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
            background: url('img/Slider-pla.jpg') no-repeat center center;
            background-size: cover;
            position: relative;
        }
        .flip-card {
            background-color: transparent;
            width: 100%;
            height: 100%;
            perspective: 1000px;
        }

        .flip-card-inner {
            position: relative;
            width: 100%;
            height: 100%;
            text-align: center;
            transition: transform 0.6s;
            transform-style: preserve-3d;
        }

        .flip-card:hover .flip-card-inner {
            transform: rotateY(180deg);
        }

        .flip-card-front, .flip-card-back {
            position: absolute;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
        }

        .flip-card-front {
            background-color: #fff;
            color: #AAD399;
            border-radius: 15px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }

        .flip-card-back {
            background-color: #f8f9fa;
            color: #AAD399;
            transform: rotateY(180deg);
            border-radius: 15px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }

        .flip-card-back .card-body::after {
            display: block;
            margin-top: 10px;
            font-size: 1.2em;
            color: #AAD399;
        }
    </style>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-4 mb-3">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="img-fluid" alt="Venus" src="<?= Yii::getAlias('@web')?>/img/DESTINOS/venus.png">
                            <h4 class="card-title">Venus</h4>
                        </div>
                        <div class="flip-card-back">
                            <div class="card-body">
                                <p>Soy un planeta fácil</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="img-fluid" alt="Marte" src="<?= Yii::getAlias('@web')?>/img/DESTINOS/marte.png">
                            <h4 class="card-title">Marte</h4>
                        </div>
                        <div class="flip-card-back">
                            <div class="card-body">
                                <p>Soy un planeta intermedio</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="img-fluid" alt="Orcus" src="<?= Yii::getAlias('@web')?>/img/DESTINOS/orcus.png">
                            <h4 class="card-title">Orcus</h4>
                        </div>
                        <div class="flip-card-back">
                            <div class="card-body">
                                <p>Soy un planeta difícil</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    <!-- LOS DADOS -->
    
    <h2 class="titulo-dado">Los dados</h2>

    <style>
        .titulo-dado {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            margin-bottom: 20px;
            margin-top:300px;
    }
    </style>
    
    <div class="container mt-5">
        <div class="dado-exp">
            Los dados son tu herramienta mas importante, con ellos podras determinar que tan efectivas con las acciones de tus jugadores, tanto en misiones pacificas, como en las batallas mas arduas.
        </div>
    </div> 
    
    <style>
        .dado-exp{
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
            background: url('img/dados.jpg') no-repeat center center;
            background-size: cover;
            position: relative;
        }
        
    </style><!-- comment -->
    
    <!-- CARRUSEL DE DADOS -->
    
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Nueva carpeta/1.jpg" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Nueva carpeta/2.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Nueva carpeta/d3.jpg" alt="Third slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Nueva carpeta/d4.jpg" alt="Third slide">
          </div><!-- comment -->
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Nueva carpeta/d5.jpg" alt="Third slide">
          </div><!-- comment -->
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= Yii::getAlias('@web')?>/img/Nueva carpeta/d6.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
        
        
        <!-- FIN CARRUSEL -->
    

        
        
        
        
        
        
        
        <!-- TIENDA -->
        

    <h2 class="titulo-tienda">La tienda</h2>

    <style>
        .titulo-tienda {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            margin-bottom: 20px;
            margin-top:50px;
    }
    </style>
    
    <div class="container mt-5">
        <div class="tienda-exp">
            En la tienda podras no solo tus jugadores podran comprobar cuanto dinero tienen en tu haber, sino adquirir por un modico precio items que les ayudaran en tu gesta.
            
        </div>
    </div> 
    
    <style>
        .tienda-exp{
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
            background: url('img/tienda.jpg') no-repeat center center;
            background-size: cover;
            position: relative;
        }
        
    </style>
    
    <div class="tienda-img" style="text-align: center;">
        <img src="<?= Yii::getAlias('@web') ?>/img/tienda2.jpg" alt="Tienda" class="small-image">
    </div>

    <style>
        .tienda-img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .small-image {
            width: 1050px; 
            height: auto; 
        }
    </style>
    
    
    
    
    
    
    
    
    <!-- La ia -->

    <h2 class="titulo-ATH">La asistente</h2>

    <style>
        .titulo-ATH {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            margin-bottom: 20px;
            margin-top:100px;
        }
    </style>
    

    <div class="container-ATH">
    <div class="izq">
        <img src="img/DESTINOS/fotico2.png" alt="Imagen" width="300px">
    </div>
    <div class="dech ATH-exp">
        <p>Entendemos que entender como funciona la app puede ser complicado, asi que para amenizar tu experiencia, ponemos a nuestra asistente ATH para que resuelva tus dudas, lo unico que debes hacer es pulsar cada vez que lo veas el botón:</p>
        <img src="img/btn.jpg" alt="btn" width="80px">
    </div>
    </div>

    <style>
        .container-ATH {
            display: flex;
            align-items: center;
        }
        .izq {
            flex: 0 0 300px;
        }
        .dech {
            flex: 1;
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
            margin-left: 30px;
            background: url('img/ATH.jpg') no-repeat center center;
            background-size: cover;
            position: relative;
        }
    </style>
    
    
    
    
    
    <!-- Criaturas -->
    
    
    <h2 class="titulo-bes">El bestiario</h2>

    <style>
        .titulo-bes {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            margin-bottom: 20px;
            margin-top:50px;
    }
    </style>
    
    <div class="container mt-5">
        <div class="best-exp">
            Por último tienes el bestiario, en el cual podras almacenar todas las criaturas que se te ocurran con las que se encontraran tus jugadores. Puedes elegir si las criaturas son amenazas o pacíficas, segun la cual han diferente cantidad de daño.
            
        </div>
    </div> 
    
    <style>
        .best-exp{
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
            background: url('img/bestiario.jpg') no-repeat center center;
            background-size: cover;
            position: relative;
        }
        
    </style>
    
    <div class="best-img" style="text-align: center;">
        <img src="<?= Yii::getAlias('@web') ?>/img/bestiario1.jpg" alt="Tienda" class="small-image">
    </div>

    <style>
        .best-img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .small-image {
            width: 1050px; 
            height: auto; 
        }
    </style>




    
        
       

        
      

         
         

    
        
        
            
        

    </div>
    
    
    
        
</div>
    
    
    
    
    
    


     
    
    
    
    



