<?php
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Bitacora;
use yii\helpers\Url;
use yii\grid\ActionColumn;
?>

<style>
    
    .grid-view th {
        background-color: transparent; 
        
        color: #F5DF61; 
        border: 2px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }

    .grid-view td {
        background-color: transparent; 
        color: #aad399; 
        
        border: 1px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }
    
</style>




<div class="meses">
    <p>
        <?= Html::a('Crear Bitacora', ['create'], ['class' => 'btn btn-createBitacora']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $bitacoras,
            'key' => 'entrada',
            'sort' => [
                'attributes' => ['fecha'],
                'defaultOrder' => [
                    'fecha' => SORT_ASC,
                ],
            ],
        ]),
        'columns' => [
            'entrada',
            'descripcion',
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d-m-y H:i:s']
            ]
            
        ],
    ]); ?>
</div>


