<?php
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Bitacora;
use yii\helpers\Url;
use yii\grid\ActionColumn;
?>

<style>
    
    .grid-view th {
        background-color: transparent; 
        
        color: #F5DF61; 
        border: 2px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }

    .grid-view td {
        background-color: transparent; 
        color: #aad399; 
        
        border: 1px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }
    
</style>




<div class="meses">
    <p>
        <?= Html::a('Create Bitacora', ['create'], ['class' => 'btn btn-createBitacora']) ?>
    </p>
    
    <style>
    .btn .btn-createBitacora {
        /* tamaño del botón */
        padding: 10px 20px; 
        font-size: 1em; 
        border: none;
        border-radius: 20px;
        background-color: #AAD399;
        color: black; 
        display: inline-block;
        cursor: pointer;
        box-shadow: 0 4px #759e64; 
        transition-duration: 0.4s;
    }

    .btn .btn-createBitacora:hover {
        background-color: #F5DF61;
        box-shadow: 0 4px #c9b649;
    }

    .btn .btn-createBitacora:active {
        background-color: #3e8e41; 
        box-shadow: 0 2px #666; 
        transform: translateY(2px); 
    }
    </style>

    <?= GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $bitacoras,
            'key' => 'entrada',
            'sort' => [
                'attributes' => ['fecha'],
                'defaultOrder' => [
                    'fecha' => SORT_ASC,
                ],
            ],
        ]),
        'columns' => [
            'entrada',
            'descripcion',
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d-m-y H:i:s']
            ]
            
        ],
    ]); ?>
</div>