<?php

use app\models\Bitacora;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;


/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Bitacoras';

?>



    <div class="container mt-5">
        <div class="bi-exp">
            <img src="<?= Yii::getAlias('@web') ?>/img/Slider-Bi.jpg" alt="Imagen" width="60px"  class="bi-fondo">
            <h2 class="titulo-bi">Bitácora</h2>
        </div>
    </div>

    <style>
        .bi-exp {
            position: relative;
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
        }
        .bi-fondo {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
            z-index: -1;
        }
        .titulo-bi {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            position: relative;
            z-index: 1;
            
        
        }
    </style>


<div class="bitacora-title" style="color: #AAD399;">
    <h3>Meses</h3>
</div>



<div class="bitacora-index">
    <div class="card">
        <div class="card-header">
            Enero
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-enero'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Febrero
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-febrero'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Marzo
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-marzo'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Abril
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-abril'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Mayo
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-mayo'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div><!-- comment -->
    
    
    <div class="card">
        <div class="card-header">
            Junio
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-junio'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Julio
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-julio'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Agosto
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-agosto'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div><!-- comment -->
    
    
    <div class="card">
        <div class="card-header">
            Septiembre
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-sept'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Octubre
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-oct'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    <div class="card">
        <div class="card-header">
            Noviembre
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-nov'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div><!-- comment -->
    
    
    <div class="card">
        <div class="card-header">
            Diciembre
        </div>
        <div class="card-body">
            <?= \yii\helpers\Html::a('Ver Días', ['bitacora/dias-dic'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    
    
    <style>
        .btn.btn-primary {
        
            padding: 10px 20px; 
            font-size: 1em; 
            border: none;
            border-radius: 20px;
            background-color: #AAD399;
            color: black; 
            display: inline-block;
            cursor: pointer;
            box-shadow: 0 4px #759e64; 
            transition-duration: 0.4s;
        }

        .btn.btn-primary:hover {
            background-color: #F5DF61;
            box-shadow: 0 4px #c9b649;
        }

        .btn.btn-primary:active {
            background-color: #3e8e41; 
            box-shadow: 0 2px #666; 
            transform: translateY(2px); 
        }
        
        .btn-fijo {
            background-color: transparent; 
            color: white; 
            border: 2px solid #aad399; 
            font-size: 16px; 
            border-radius: 15px; 
            position: fixed;
            bottom: 50px;
            right: 30px;
            z-index: 100;
            padding: 20px 40px; 
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            transition: transform 0.2s ease-in-out;
            box-shadow: 0 0 10px rgba(0, 255, 0, 0.5);     
        }


        .btn-fijo-site:hover {
            transform: scale(1.1);
        }
    </style>
    
    
   
    
    
    
    
    
    
    
    
    
    
    
<!-- BOTON MASCOTA -->
<?= Html::a('?', [''], ['class' => 'btn  btn-fijo', 'id' => 'boton-toggle']) ?>



<!-- CONTENEDOR MASCOTA -->
<div id="imagen-container" style="display: none; position: fixed; bottom: 100px; right: 30px; z-index: 101;">
    <img class="d-block w-1" src="<?= Yii::getAlias('@web')?>/img/ATH/ATH_BI.png" style="width: 500px; height: auto;">
</div>




<!-- JS MANEJAR MASCOTA -->
<script>
document.getElementById('boton-toggle').addEventListener('click', function(event) {
    // Previene el comportamiento por defecto del enlace
    event.preventDefault();
    
    var boton = this;
    var imagenContainer = document.getElementById('imagen-container');
    
    // Verifica si la imagen ya está visible
    if (imagenContainer.style.display === 'none') {
        // Muestra la imagen
        imagenContainer.style.display = 'block';
        
        // Cambia la apariencia del botón a 'X'
        boton.innerHTML = 'X';
    } else {
        // Oculta la imagen
        imagenContainer.style.display = 'none';
        
        // Restablece la apariencia del botón a '?'
        boton.innerHTML = '?';
    }
});
</script>
    
</div>






