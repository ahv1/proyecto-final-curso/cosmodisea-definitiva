<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Bitacora $model */

$this->title = 'Update Bitacora: ' . $model->entrada;
$this->params['breadcrumbs'][] = ['label' => 'Bitacoras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->entrada, 'url' => ['view', 'entrada' => $model->entrada]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bitacora-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
