<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Bitacora $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="bitacora-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'entrada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->input('datetime-local', ['class' => 'form-control', 'placeholder' => 'YYYY-MM-DDTHH:MM:SS']) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>






