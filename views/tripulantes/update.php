<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\tripulantes $model */

$this->title = 'Update Tripulantes: ' . $model->n_jugador;

?>
<div class="tripulantes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>

</div>
