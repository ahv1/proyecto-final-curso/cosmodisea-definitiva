<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/** @var yii\web\View $this */
/** @var app\models\tripulantes $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tripulantes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    
    if ($model->isNewRecord) {
        echo $form->field($model, 'n_jugador')->textInput(['maxlength' => true]);
    } else {
        echo $form->field($model, 'n_jugador')->textInput(['readonly' => true]);
    }
    ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'origen')->textarea(['rows' => 6]) ?>

    




    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

