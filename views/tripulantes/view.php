<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\tripulantes $model */

$this->title = $model->n_jugador;
$this->params['breadcrumbs'][] = ['label' => 'Tripulantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tripulantes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'n_jugador' => $model->n_jugador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'n_jugador' => $model->n_jugador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'n_jugador',
            'nombre',
            'nivel',
            'origen:ntext',
            'raza',
        ],
    ]) ?>
    

</div>
