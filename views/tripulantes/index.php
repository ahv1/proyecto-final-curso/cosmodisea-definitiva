<?php

use app\models\clases;
use app\models\tripulantes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;


/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider 

$this->title = 'Tripulantes';
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="tripulantes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    
        

        
    <div class="container mt-5">
        <div class="tri-exp">
            <img src="<?= Yii::getAlias('@web') ?>/img/Slider-tri.jpg" alt="Imagen" width="60px"  class="tri-fondo">
            <h2 class="titulo-tripulantes">Tus tripulantes</h2>
        </div>
    </div>

    <style>
        .tri-exp {
            position: relative;
            border: 3px solid #AAD399;
            padding: 20px;
            color: #AAD399;
            box-shadow: 0 0 20px rgba(0, 255, 204, 0.5);
            text-shadow: 0 0 5px #AAD399;
            margin-bottom: 50px;
        }
        .tri-fondo {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
            z-index: -1;
        }
        .titulo-tripulantes {
            color: #F5DF61;
            text-shadow: 0 0 10px #F5DF61;
            position: relative;
            z-index: 1;
        }
    </style>

    <p>
        <?= Html::a('Crear Tripulantes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <?= Html::a('Asignar Clases', ['/clases/create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    
    <style>
        
    .btn.btn-success {
        
        padding: 10px 20px; 
        font-size: 1em; 
        border: none;
        border-radius: 20px;
        background-color: #AAD399;
        color: black; 
        display: inline-block;
        cursor: pointer;
        box-shadow: 0 4px #759e64; 
        transition-duration: 0.4s;
    }

    .btn.btn-success:hover {
        background-color: #F5DF61;
        box-shadow: 0 4px #c9b649;
    }

    .btn.btn-success:active {
        background-color: #3e8e41; 
        box-shadow: 0 2px #666; 
        transform: translateY(2px); 
    }
    
    

    
    .grid-view th {
        background-color: transparent; 
        color: #aad399; 
        border: 2px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }

    .grid-view td {
        background-color: transparent; 
        border: 1px solid #aad399; 
        box-shadow: 0 0 5px #aad399; 
        padding: 10px 20px; 
        font-size: 16px; 
        font-weight: bold; 
        border-radius: 15px; 
        box-shadow: 0 0 10px rgba(0, 255, 0, 0.5); 
        
    }
    
 
    
    
    .tripulante-container {
        background-color: transparent;
        color: #aad399;
        border: 1px solid #aad399;
        box-shadow: 0 0 20px #aad399;
        padding: 10px 20px;
        font-size: 16px;
        font-weight: bold;
        border-radius: 15px;
        word-wrap: break-word; 
        margin-bottom: 20px; 

        transition: transform 0.5s ease, box-shadow 0.5s ease;
    }

    .tripulante-title {
        color: #f5df61; 
        font-size: 24px;
    }

    .barra-vida-container {
        background-color: #aad399; 
        border-radius: 5px;
        margin: 10px 0;
        box-shadow: 0 0 10px #aad399;
    }

    .barra-vida {
        background-color: #aad399; 
        height: 20px;
        border-radius: 5px;
        text-align: center;
        line-height: 20px;
        color: #aad399; 
    }

    .tripulante-actions a {
        color: #f5df61; 
        text-decoration: none;
        margin-right: 10px;
    }


    @keyframes pulsate {
            0% { box-shadow: 0 0 5px #30cfd0; }
            50% { box-shadow: 0 0 20px #30cfd0; }
            100% { box-shadow: 0 0 5px #30cfd0; }
        }

    .tripulante-container:hover {
            animation: pulsate 2s infinite;
        }    


</style>
    
    
    
    
    
    
    
    
    
    
    
    <?= \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model) {
        
        return "<div class='tripulante-container'>
                    <div class='tripulante-title'>{$model->nombre}</div>
                    <p><strong>Nombre de Jugador:</strong> {$model->n_jugador}</p>
                    <p><strong>Nivel:</strong> {$model->nivel}</p>
                    <p><strong>Origen:</strong> {$model->origen}</p>
                    <p><strong>Clases:</strong> " . implode(', ', ArrayHelper::map($model->clases, 'id', 'clase')) . "</p>
                    
                    <div class='tripulante-actions'>" .
                        
                        Html::a(Html::img('@web/img/ICONS/update.png'), ['update', 'n_jugador' => $model->n_jugador]) . " " .
                        Html::a(Html::img('@web/img/ICONS/borrar.png'), ['delete', 'n_jugador' => $model->n_jugador], [
                            'data' => [
                                'confirm' => '¿Estás seguro de que quieres eliminar este elemento?',
                                'method' => 'post',
                            ],
                        ]) . "
                    </div>
                </div>";
    },
    'layout' => "{summary}\n{items}\n{pager}",
]);
?>






    
    








</div>









<!-- BOTON MASCOTA -->
<?= Html::a('?', [''], ['class' => 'btn btn-primary btn-fijo', 'id' => 'boton-toggle']) ?>

<!-- CONTENEDOR MASCOTA -->
<div id="imagen-container" style="display: none; position: fixed; bottom: 100px; right: 30px; z-index: 101; background: transparent;">
    <img id="imagen-mascota" class="d-block w-1" src="<?= Yii::getAlias('@web')?>/img/ATH/ATH_TRI.png" style="width: 500px; height: auto;">
    <!-- BOTON SIGUIENTE -->
    <button id="boton-siguiente" class="btn btn-secondary">Siguiente</button>
</div>

<!-- JS MANEJAR MASCOTA -->
<script>
    document.getElementById('boton-toggle').addEventListener('click', function(event) {
        event.preventDefault();

        var boton = this;
        var imagenContainer = document.getElementById('imagen-container');

        if (imagenContainer.style.display === 'none') {
            imagenContainer.style.display = 'block';
            boton.innerHTML = 'X';
        } else {
            imagenContainer.style.display = 'none';
            boton.innerHTML = '?';
        }
    });

    // Array de imágenes
    var imagenes = [
        "<?= Yii::getAlias('@web')?>/img/ATH/ATH_TRI.png",

        "<?= Yii::getAlias('@web')?>/img/ATH/ATH_NIVELES.png"
    ];
    var indiceImagenActual = 0;

    // Función para cambiar la imagen
    function cambiarImagen() {
        indiceImagenActual = (indiceImagenActual + 1) % imagenes.length;
        document.getElementById('imagen-mascota').src = imagenes[indiceImagenActual];
    }

    // Evento para el botón Siguiente
    document.getElementById('boton-siguiente').addEventListener('click', cambiarImagen);
</script>