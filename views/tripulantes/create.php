<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** 

$this->title = 'Create Tripulantes';
$this->params['breadcrumbs'][] = ['label' => 'Tripulantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;@var app\models\tripulantes $model */
$this->title = 'Crear Tripulantes';

?>
<div class="tripulantes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>
    
    

</div>
