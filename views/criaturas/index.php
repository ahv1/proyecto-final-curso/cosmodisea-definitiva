<?php

use app\models\criaturas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider 

$this->title = 'Criaturas';
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="criaturas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Criaturas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    
  <style>
 
        .btn.btn-success {
        
            padding: 10px 20px; 
            font-size: 1em; 
            border: none;
            border-radius: 20px;
            background-color: #AAD399;
            color: black; 
            display: inline-block;
            cursor: pointer;
            box-shadow: 0 4px #759e64; 
            transition-duration: 0.4s;
        }

        .btn.btn-success:hover {
            background-color: #F5DF61;
            box-shadow: 0 4px #c9b649;
        }

        .btn.btn-success:active {
            background-color: #3e8e41; 
            box-shadow: 0 2px #666; 
            transform: translateY(2px); 
        }
      
    
      
    
    
    .criatura-container {
        background-color: transparent;
        color: #aad399;
        border: 1px solid #aad399;
        box-shadow: 0 0 20px #aad399;
        padding: 10px 20px;
        font-size: 16px;
        font-weight: bold;
        border-radius: 15px;
        word-wrap: break-word; 
        margin-bottom: 20px; 

        transition: transform 0.5s ease, box-shadow 0.5s ease;
    }

    .criatura-title {
        color: #f5df61; ; 
        font-size: 24px;
    }



    .criatura-actions a {
        color: #f5df61; 
        text-decoration: none;
        margin-right: 10px;
    }


    @keyframes pulsate {
            0% { box-shadow: 0 0 5px #30cfd0; }
            50% { box-shadow: 0 0 20px #30cfd0; }
            100% { box-shadow: 0 0 5px #30cfd0; }
        }

    .criatura-container:hover {
            animation: pulsate 2s infinite;
        }    
    

</style>
    
    
    
    
    
    
    
    
    
    
    
    <?= \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model) {
        
        return "<div class='criatura-container'>
                    <div class='criatura-title'>{$model->nom_criaturas}</div>
                    <p><strong>Tipo:</strong> {$model->tipo}</p>
                    <p><strong>Descripción:</strong> {$model->descripcion}</p>
                    
                    
                    <div class='criatura-actions'>" .
                        Html::a(Html::img('@web/img/ICONS/update.png'), ['update', 'cod_cria' => $model->cod_cria]) . " " .
                        Html::a(Html::img('@web/img/ICONS/borrar.png'), ['delete', 'cod_cria' => $model->cod_cria], [
                            'data' => [
                                'confirm' => '¿Estás seguro de que quieres eliminar este elemento?',
                                'method' => 'post',
                            ],
                        ]) . "
                    </div>
                </div>";
    },
    'layout' => "{summary}\n{items}\n{pager}",
]);
?>






    
    








</div>









