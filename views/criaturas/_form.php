<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\criaturas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="criaturas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nom_criaturas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tipo')->dropDownList(
        ['pacifico' => 'Pacífico', 'amenaza' => 'Amenaza'], // Opciones del desplegable
        ['prompt' => 'Selecciona un tipo de criatura'] // Texto del placeholder
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
