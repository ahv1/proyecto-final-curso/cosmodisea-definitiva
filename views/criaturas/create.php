<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\criaturas $model */

$this->title = 'Create Criaturas';
$this->params['breadcrumbs'][] = ['label' => 'Criaturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="criaturas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
