<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\criaturas $model */

$this->title = $model->cod_cria;
$this->params['breadcrumbs'][] = ['label' => 'Criaturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="criaturas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_cria' => $model->cod_cria], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_cria' => $model->cod_cria], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_cria',
            'nom_criaturas',
            'descripcion:ntext',
            'tipo',
            'vida_cria',
        ],
    ]) ?>

</div>
