<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\criaturas $model */

$this->title = 'Update Criaturas: ' . $model->cod_cria;
$this->params['breadcrumbs'][] = ['label' => 'Criaturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_cria, 'url' => ['view', 'cod_cria' => $model->cod_cria]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="criaturas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
