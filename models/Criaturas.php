<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "criaturas".
 *
 * @property int $cod_cria
 * @property string|null $nom_criaturas
 * @property string|null $descripcion
 * @property string|null $tipo
 * @property int|null $vida_cria
 *
 * @property Registran[] $registrans
 */
class Criaturas extends \yii\db\ActiveRecord
{
    /**
    public $maxVida = 100;
    
    
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'criaturas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['vida_cria'], 'integer'],
            [['nom_criaturas', 'tipo'], 'string', 'max' => 20],
        ];
    }
    
    public function init()
    {
        parent::init();
        
        $this->vida_cria = 100;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_cria' => 'Cod Cria',
            'nom_criaturas' => 'Nom Criaturas',
            'descripcion' => 'Descripcion',
            'tipo' => 'Tipo',
            'vida_cria' => 'Vida Cria',
        ];
    }

    /**
     * Gets query for [[Registrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrans()
    {
        return $this->hasMany(Registran::class, ['cod_cria' => 'cod_cria']);
    }
    
    
    public function porcentajeVida_cria() {
        
        return ($this->vida_cria / 30) * 100;
    }
    
    
    
}
