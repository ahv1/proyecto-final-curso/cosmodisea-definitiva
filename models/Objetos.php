<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetos".
 *
 * @property int $cod_items
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $precio
 *
 * @property Compran[] $comprans
 */
class Objetos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion', 'precio'], 'required'],
            [['descripcion'], 'string'],
            [['precio'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_items' => 'Cod Items',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::class, ['cod_items' => 'cod_items']);
    }
    
    
    
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->cod_items = $this->generateUniqueCodItems();
        }
        return parent::beforeSave($insert);
    }

    private function generateUniqueCodItems()
    {
        do {
            $randomNumber = rand(1000, 9999); // Genera un número aleatorio de 4 dígitos
        } while (self::find()->where(['cod_items' => $randomNumber])->exists());

        return $randomNumber;
    }
}
