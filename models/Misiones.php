<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "misiones".
 *
 * @property string $nom_mis
 * @property string|null $informe
 * @property int|null $dificultad
 * @property int|null $recompensa
 *
 * @property Realizan[] $realizans
 */
class Misiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'misiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nom_mis'], 'required'],
            [['informe'], 'string'],
            // Añade la regla 'in' para 'dificultad' y 'recompensa'
            [['dificultad'], 'in', 'range' => [1, 2, 3]],
            [['recompensa'], 'in', 'range' => [1]],
            [['nom_mis'], 'string', 'max' => 30],
            [['nom_mis'], 'unique'],
            [['pago'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nom_mis' => 'Nom Mis',
            'informe' => 'Informe',
            'dificultad' => 'Dificultad',
            'recompensa' => 'Recompensa',
            'pago' => 'Pago',
        ];
    }
    
    // Establece la recompensa por defecto en 1
    public function init()
    {
        parent::init();
        
        $this->recompensa = 1;
    }

    /**
     * Gets query for [[Realizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizans()
    {
        return $this->hasMany(Realizan::class, ['nom_mis' => 'nom_mis']);
    }
    
    
    
    
    
}
