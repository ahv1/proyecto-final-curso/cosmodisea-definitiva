<?php

namespace app\models;

use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property int $id
 * @property string|null $n_jugador
 * @property string|null $clase
 *
 * @property Tripulantes $nJugador
 * @property Tripulantes $nJugador0
 */
class Clases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_jugador'], 'string', 'max' => 20],
            [['clase'], 'string', 'max' => 30],
            [['n_jugador', 'clase'], 'unique', 'targetAttribute' => ['n_jugador', 'clase']],
            [['n_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Tripulantes::class, 'targetAttribute' => ['n_jugador' => 'n_jugador']],
            [['n_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Tripulantes::class, 'targetAttribute' => ['n_jugador' => 'n_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'n_jugador' => 'N Jugador',
            'clase' => 'Clase',
        ];
    }

    /**
     * Gets query for [[NJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNJugador()
    {
        return $this->hasOne(Tripulantes::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[NJugador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNJugador0()
    {
        return $this->hasOne(Tripulantes::class, ['n_jugador' => 'n_jugador']);
    }
    
    
    public function getTripulantesList()
    {
        $tripulantes = Tripulantes::find()->all();
        $listData = ArrayHelper::map($tripulantes, 'n_jugador', 'n_jugador');
        return $listData;
    }

}
