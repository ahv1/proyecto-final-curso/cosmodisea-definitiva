<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetos_comprados".
 *
 * @property int $id
 * @property int|null $cod_items
 * @property int|null $disponible
 *
 * @property Objetos $codItems
 */
class ObjetosComprados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetos_comprados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_items', 'disponible'], 'integer'],
            [['cod_items'], 'exist', 'skipOnError' => true, 'targetClass' => Objetos::class, 'targetAttribute' => ['cod_items' => 'cod_items']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_items' => 'Cod Items',
            'disponible' => 'Disponible',
        ];
    }

    /**
     * Gets query for [[CodItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodItems()
    {
        return $this->hasOne(Objetos::class, ['cod_items' => 'cod_items']);
    }
    
    
    
    public function getObjeto()
    {
        return $this->hasOne(Objetos::className(), ['cod_items' => 'cod_items']);
    }
}
