<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "realizan".
 *
 * @property int $id
 * @property string|null $n_jugador
 * @property string|null $nom_mis
 *
 * @property Tripulantes $nJugador
 * @property Misiones $nomMis
 */
class Realizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'realizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_jugador'], 'string', 'max' => 20],
            [['nom_mis'], 'string', 'max' => 30],
            [['nom_mis'], 'exist', 'skipOnError' => true, 'targetClass' => Misiones::class, 'targetAttribute' => ['nom_mis' => 'nom_mis']],
            [['n_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Tripulantes::class, 'targetAttribute' => ['n_jugador' => 'n_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'n_jugador' => 'N Jugador',
            'nom_mis' => 'Nom Mis',
        ];
    }

    /**
     * Gets query for [[NJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNJugador()
    {
        return $this->hasOne(Tripulantes::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[NomMis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomMis()
    {
        return $this->hasOne(Misiones::class, ['nom_mis' => 'nom_mis']);
    }
}
