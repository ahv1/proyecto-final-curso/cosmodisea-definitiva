<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monedero".
 *
 * @property int $cod_mon
 * @property int|null $cantidad
 *
 * @property TripulanteMonedero[] $tripulanteMonederos
 */
class Monedero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monedero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_mon'], 'required'],
            [['cod_mon', 'cantidad'], 'integer'],
            [['cod_mon'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_mon' => 'Cod Mon',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[TripulanteMonederos]].
     *
     * @return \yii\db\ActiveQuery
     */
    
}
