<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bitacora".
 *
 * @property string $entrada
 * @property string $fecha
 * @property string|null $descripcion
 */
class Bitacora extends \yii\db\ActiveRecord
{
    
    public $vista; // Atributo temporal para la vista
    
    
    
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitacora';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entrada'], 'required'],
            [['fecha'], 'safe'],
            [['descripcion'], 'string'],
            [['entrada'], 'string', 'max' => 20],
            [['entrada'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entrada' => 'Entrada',
            'fecha' => 'Fecha',
            'descripcion' => 'Descripcion',
        ];
    }
    
    
    
    public static function findByMes($mes)
    {
        // Convierte el nombre del mes a su número correspondiente
        $mesNumero = date('m', strtotime($mes . ' 1 2024'));

        // Busca todas las entradas de cada mes sin importar el año
        return self::find()
            ->where(['MONTH(fecha)' => $mesNumero])
            ->all();
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    



}
