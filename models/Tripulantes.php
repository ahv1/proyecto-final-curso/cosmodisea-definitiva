<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tripulantes".
 *
 * @property string $n_jugador
 * @property string|null $nombre
 * @property int|null $nivel
 * @property int|null $vida
 * @property string|null $origen
 *
 * @property Clases[] $clases
 * @property Clases[] $clases0
 * @property Compran[] $comprans
 * @property Escriben[] $escribens
 * @property Realizan[] $realizans
 * @property Registran[] $registrans
 * @property TripulanteMonedero $tripulanteMonedero
 */
class Tripulantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tripulantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_jugador', 'nombre', 'origen'], 'required'],
            [['nivel', 'vida'], 'integer'],
            [['origen'], 'string'],
            [['n_jugador'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 30],
            [['n_jugador'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'n_jugador' => 'N Jugador',
            'nombre' => 'Nombre',
            'nivel' => 'Nivel',
            'vida' => 'Vida',
            'origen' => 'Origen',
        ];
    }
    // Establezco un valor nicial para el nivel y la vida
    public function init()
    {
        parent::init();
        
        $this->nivel = 1;
        
        $this->vida = 100;
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::class, ['n_jugador' => 'n_jugador']);
    }
    
    

    /**
     * Gets query for [[Clases0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases0()
    {
        return $this->hasMany(Clases::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[Escribens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribens()
    {
        return $this->hasMany(Escriben::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[Realizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizans()
    {
        return $this->hasMany(Realizan::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[Registrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrans()
    {
        return $this->hasMany(Registran::class, ['n_jugador' => 'n_jugador']);
    }

    /**
     * Gets query for [[TripulanteMonedero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTripulanteMonedero()
    {
        return $this->hasOne(TripulanteMonedero::class, ['n_jugador' => 'n_jugador']);
    }
    
    
    //funcion que establece medida de salud de tripulantes
    public function getPorcentajeVida() {
        
        return ($this->vida / 30) * 100;
    }
}
