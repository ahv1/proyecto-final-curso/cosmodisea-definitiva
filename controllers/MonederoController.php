<?php

namespace app\controllers;

use app\models\monedero;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonederoController implements the CRUD actions for monedero model.
 */
class MonederoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all monedero models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => monedero::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_mon' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single monedero model.
     * @param int $cod_mon Cod Mon
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_mon)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_mon),
        ]);
    }

    /**
     * Creates a new monedero model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new monedero();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_mon' => $model->cod_mon]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing monedero model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_mon Cod Mon
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_mon)
    {
        $model = $this->findModel($cod_mon);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_mon' => $model->cod_mon]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing monedero model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_mon Cod Mon
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_mon)
    {
        $this->findModel($cod_mon)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the monedero model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_mon Cod Mon
     * @return monedero the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_mon)
    {
        if (($model = monedero::findOne(['cod_mon' => $cod_mon])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
