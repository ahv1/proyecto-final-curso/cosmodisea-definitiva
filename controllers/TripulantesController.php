<?php

namespace app\controllers;

use Yii;
use app\models\tripulantes;
use app\models\Clases;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TripulantesController implements the CRUD actions for tripulantes model.
 */
class TripulantesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all tripulantes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => tripulantes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'n_jugador' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single tripulantes model.
     * @param string $n_jugador N Jugador
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($n_jugador)
    {
        return $this->render('view', [
            'model' => $this->findModel($n_jugador),
        ]);
    }

    /**
     * Creates a new tripulantes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Tripulantes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //redirige al índice de tripulantes en lugar de la vista del tripulante
            return $this->redirect(['tripulantes/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing tripulantes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $n_jugador N Jugador
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($n_jugador)
    {
        $model = $this->findModel($n_jugador);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index', 'n_jugador' => $model->n_jugador]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    

    /**
     * Deletes an existing tripulantes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $n_jugador N Jugador
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
    
    public function actionDelete($n_jugador)
    {
        $this->findModel($n_jugador)->delete();

        return $this->redirect(['index']);
    }
 */
    /**
     * Finds the tripulantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $n_jugador N Jugador
     * @return tripulantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($n_jugador)
    {
        if (($model = tripulantes::findOne(['n_jugador' => $n_jugador])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    public function actionDelete($n_jugador)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $clases = Clases::findAll(['n_jugador' => $n_jugador]);
            foreach ($clases as $clase) {
                $clase->delete();
            }
            $this->findModel($n_jugador)->delete();
            $transaction->commit();
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    
    
    
    
    /**
     * Finds the tripulantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $n_jugador N Jugador
     * @return tripulantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     
    
    public function actionReducirVida() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $n_jugador = Yii::$app->request->post('n_jugador');
            $tripulante = Tripulantes::findOne(['n_jugador' => $n_jugador]);
            if ($tripulante && $tripulante->vida > 0) {
                $tripulante->vida--;
                if ($tripulante->save()) {
                    return ['success' => true, 'vida' => $tripulante->vida];
                }
            }
            return ['success' => false];
        }
        throw new BadRequestHttpException('Solicitud no permitida');
    }
    */
    
    
}
