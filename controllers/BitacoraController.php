<?php

namespace app\controllers;

use app\models\Bitacora;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BitacoraController implements the CRUD actions for Bitacora model.
 */
class BitacoraController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Bitacora models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Bitacora::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'entrada' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
   
    

    /**
     * Displays a single Bitacora model.
     * @param string $entrada Entrada
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($entrada)
    {
        return $this->render('view', [
            'model' => $this->findModel($entrada),
        ]);
    }

    /**
     * Creates a new Bitacora model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Bitacora();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index', 'entrada' => $model->entrada]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Bitacora model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $entrada Entrada
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($entrada)
    {
        $model = $this->findModel($entrada);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'entrada' => $model->entrada]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bitacora model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $entrada Entrada
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($entrada)
    {
        $this->findModel($entrada)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bitacora model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $entrada Entrada
     * @return Bitacora the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($entrada)
    {
        if (($model = Bitacora::findOne(['entrada' => $entrada])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    
    
    
    /** EMPIEZAN CONSULTAS CALENDARIO*/
    
    public function actionDiasEnero() {
        $bitacoras = Bitacora::findByMes('January');
        return $this->render('dias_enero', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasFebrero() {
        $bitacoras = Bitacora::findByMes('February');
        return $this->render('dias_febrero', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasMarzo() {
        $bitacoras = Bitacora::findByMes('March');
        return $this->render('dias_marzo', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasAbril() {
        $bitacoras = Bitacora::findByMes('April');
        return $this->render('dias_abril', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasMayo() {
        $bitacoras = Bitacora::findByMes('May');
        return $this->render('dias_mayo', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasJunio() {
        $bitacoras = Bitacora::findByMes('June');
        return $this->render('dias_junio', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasJulio() {
        $bitacoras = Bitacora::findByMes('July');
        return $this->render('dias_julio', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasAgosto() {
        $bitacoras = Bitacora::findByMes('August');
        return $this->render('dias_agosto', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasSept() {
        $bitacoras = Bitacora::findByMes('September');
        return $this->render('dias_sept', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasOct() {
        $bitacoras = Bitacora::findByMes('October');
        return $this->render('dias_oct', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasNov() {
        $bitacoras = Bitacora::findByMes('November');
        return $this->render('dias_nov', ['bitacoras' => $bitacoras]);
    }
    
    public function actionDiasDic() {
        $bitacoras = Bitacora::findByMes('December');
        return $this->render('dias_dic', ['bitacoras' => $bitacoras]);
    }
    
    
    
    
    
    
    /**EMPIEZAN CONSULTAS DE VIEW OR CREATE*/
    
    
    
    
    
}
