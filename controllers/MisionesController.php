<?php

namespace app\controllers;


use app\models\misiones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Monedero;


use app\models\Tripulantes;

/**
 * MisionesController implements the CRUD actions for misiones model.
 */
class MisionesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all misiones models.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $misiones = Misiones::find()->all();
        $tripulantes = Tripulantes::find();
        $dataProvider = new ActiveDataProvider([
            
            'query' => misiones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nom_mis' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'misiones' => $misiones,
            'tripulantes' => $tripulantes,
            
        ]);
    }


    /**
     * Displays a single misiones model.
     * @param string $nom_mis Nom Mis
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nom_mis)
    {
        return $this->render('view', [
            'model' => $this->findModel($nom_mis),
        ]);
    }
    
    
    
    public function actionViewPlanetas($cod_planetas)
    {
        $misiones = Mision::findAll(['cod_planetas' => $cod_planetas]);

        return $this->render('view', [
            'misiones' => $misiones,
        ]);
    }
    
    
    
    
    
    
    
    

    /**
     * Creates a new misiones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new misiones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index', 'nom_mis' => $model->nom_mis]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing misiones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nom_mis Nom Mis
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nom_mis)
    {
        $model = $this->findModel($nom_mis);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index', 'nom_mis' => $model->nom_mis]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing misiones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nom_mis Nom Mis
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nom_mis)
    {
        $this->findModel($nom_mis)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the misiones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nom_mis Nom Mis
     * @return misiones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nom_mis)
    {
        if (($model = misiones::findOne(['nom_mis' => $nom_mis])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    
    
    
    
    
    
    
    public function actionCompletar($nom_mis)
    {
        // Obtener la misión
        $mision = Misiones::findOne($nom_mis);

        if ($mision) {
            // Obtener la recompensa y el pago de la misión
            $recompensa = $mision->recompensa;
            $pago = $mision->pago;

            // Actualizar el nivel de los tripulantes
            Tripulantes::updateAllCounters(['nivel' => $recompensa]);

            // Obtener el monedero con cod_mon = 1
            $monedero = Monedero::findOne(1);

            // Verificar si el monedero existe
            if ($monedero) {
                // Sumar el pago al monedero
                $monedero->cantidad += $pago;
                // Guardar los cambios en el monedero
                $monedero->save();
            } else {
                // Si no existe el monedero, puedes decidir qué acción tomar
                Yii::$app->session->setFlash('error', 'El monedero no existe.');
            }

            // Redirigir a la vista de misiones
            return $this->redirect(['misiones/index']);
        } else {

            Yii::$app->session->setFlash('error', 'La misión no existe.');
            return $this->redirect(['misiones/index']);
        }
    }
    
    
    
    public function actionMostrar()
    {
        //  modelo Misiones está cargado y accesible aquí
        $misionesNivel1 = Misiones::find()->where(['dificultad' => 1])->all();

        return $this->render('mostrar', [
            'misionesNivel1' => $misionesNivel1,
        ]);
    }
    
    
    
    public function actionMostrar1()
    {
        $misionesNivel2 = Misiones::find()->where(['dificultad' => 2])->all();

        return $this->render('mostrar1', [
            'misionesNivel2' => $misionesNivel2,
        ]);
    }



    public function actionMostrar2()
    {
        $misionesNivel3 = Misiones::find()->where(['dificultad' => 3])->all();

        return $this->render('mostrar2', [
            'misionesNivel3' => $misionesNivel3,
        ]);
    }


    
    

    
    
    
    
    
    
}
