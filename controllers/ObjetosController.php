<?php

namespace app\controllers;

use app\models\objetos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Monedero;
use app\models\ObjetosComprados;


/**
 * ObjetosController implements the CRUD actions for objetos model.
 */
class ObjetosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all objetos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $monedero = $this->findMonedero(1);
        $objetos = Objetos::find()->all();
        $objetosComprados = ObjetosComprados::find()->all();
        $dataProvider = new ActiveDataProvider([
            'query' => objetos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_items' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'objetos' => $objetos,
            'dataProvider' => $dataProvider,
            'monedero' => $monedero,
            'objetosComprados' => $objetosComprados,

            
        ]);
    }

    /**
     * Displays a single objetos model.
     * @param int $cod_items Cod Items
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_items)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_items),
        ]);
    }

    /**
     * Creates a new objetos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new objetos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index', 'cod_items' => $model->cod_items]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing objetos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_items Cod Items
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_items)
    {
        $model = $this->findModel($cod_items);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_items' => $model->cod_items]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing objetos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_items Cod Items
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_items)
    {
        $this->findModel($cod_items)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the objetos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_items Cod Items
     * @return objetos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     
     * 
     * 
     * 
     * 
     * 
     * 
    protected function findModel($cod_items)
    {
        if (($model = objetos::findOne(['cod_items' => $cod_items])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    */
    
    
    

    protected function findModel($cod_items)
    {
        if (($model = Objetos::findOne($cod_items)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('El objeto solicitado no existe.');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function actionComprar()
    {
        //Obtiene el cod_item 
        $cod_items = \Yii::$app->request->post('cod_items');
        //Inicializa la transaccion de la base de datos
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            //procesa la compra y obtiene el objetos
            $objeto = $this->procesarCompra($cod_items);
            //confirma transaccion
            $transaction->commit();
            //transmite el success
            \Yii::$app->session->setFlash('success', 'Compra realizada con éxito.');

            //agrega el objeto comprado a la sesión
            $objetosComprados = \Yii::$app->session->get('objetosComprados', []);
            $objetosComprados[] = $objeto;
            \Yii::$app->session->set('objetosComprados', $objetosComprados);

            
        //maneja el error y revierte la transaccion
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::$app->session->setFlash('error', 'Ha ocurrido un error al realizar la compra.');
        }
        
        return $this->redirect(['index']);
    }


    private function procesarCompra($cod_items)
    {
        //busca el modelo del objeto
        $objeto = $this->findModel($cod_items);
        $monedero = $this->findMonedero(1); // Asumiendo que el cod_mon del monedero es 1

        //verifica si hay dinero suficiente en el monedero
        if (!$monedero || $monedero->cantidad < $objeto->precio) {
            throw new \Exception('Fondos insuficientes.');
        }
        
        //descuenta el precio del objeto al monedero
        $monedero->cantidad -= $objeto->precio;
        
        //guarda la nueva cantidad de dinero
        if (!$monedero->save()) {
            throw new \Exception('Error al guardar el monedero.');
        }

        //crea un registro de objetos comprados
        $objetoComprado = new ObjetosComprados();
        $objetoComprado->cod_items = $cod_items;
        //establece el objeto como disponible
        $objetoComprado->disponible = 1; 
        //guarda el objeto comprado
        if (!$objetoComprado->save()) {
            throw new \Exception('Error al guardar en objetos_comprados.');
        }
    }

    //busca el monedero
    private function findMonedero($cod_mon)
    {
        if (($monedero = Monedero::findOne($cod_mon)) !== null) {
            return $monedero;
        }
        throw new NotFoundHttpException('El monedero no existe.');
    }

    
    






    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
