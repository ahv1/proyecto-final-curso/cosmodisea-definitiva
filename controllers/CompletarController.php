<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
// ... otros 'use' necesarios para tu controlador

class CompletarController extends Controller
{
    
    public function actionCompletar($nom_mision)
{
    $transaction = Yii::$app->db->beginTransaction();
    try {
        $mision = Misiones::findOne($nom_mision);
        if (!$mision) {
            throw new NotFoundHttpException("La misión no existe.");
        }

        $tripulantes = Tripulantes::find()->all();
        foreach ($tripulantes as $tripulante) {
            $tripulante->nivel += $mision->recompensa;
            $tripulante->save();
        }

        $transaction->commit();
        Yii::$app->session->setFlash('success', 'Misión completada y niveles actualizados.');
    } catch (\Exception $e) {
        $transaction->rollBack();
        Yii::$app->session->setFlash('error', 'Ocurrió un error al completar la misión.');
    }

    return $this->redirect(['index']); 
}

    
}



