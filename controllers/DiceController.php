<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class DiceController extends Controller
{
    public function actionLanzarDado()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resultado = rand(1, 20);
        return ['resultado' => $resultado];
    }
}



