<?php

namespace app\controllers;

use app\models\criaturas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CriaturasController implements the CRUD actions for criaturas model.
 */
class CriaturasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all criaturas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => criaturas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_cria' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single criaturas model.
     * @param int $cod_cria Cod Cria
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_cria)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_cria),
        ]);
    }

    /**
     * Creates a new criaturas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new criaturas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index', 'cod_cria' => $model->cod_cria]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing criaturas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_cria Cod Cria
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_cria)
    {
        $model = $this->findModel($cod_cria);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index', 'cod_cria' => $model->cod_cria]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing criaturas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_cria Cod Cria
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_cria)
    {
        $this->findModel($cod_cria)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the criaturas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_cria Cod Cria
     * @return criaturas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_cria)
    {
        if (($model = criaturas::findOne(['cod_cria' => $cod_cria])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
