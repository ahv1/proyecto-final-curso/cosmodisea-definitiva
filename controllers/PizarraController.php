<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider; 
use app\models\Tripulantes; 
use app\models\ObjetosComprados;

class PizarraController extends Controller
{
    public function actionIndex()
    {
        $tripulantes = Tripulantes::find()->all();
        $objetosComprados = ObjetosComprados::find()->where(['disponible' => 1])->all();
        $dataProvider = new ActiveDataProvider([
            'query' => Tripulantes::find(),
            
        ]);

        return $this->render('index', [
            'tripulantes' => $tripulantes,
            'objetosComprados' => $objetosComprados,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    
    public function actionUsar($id)
    {
        $objetoComprado = ObjetosComprados::findOne($id);
        if ($objetoComprado) {
            $objetoComprado->delete();
        }
        return $this->redirect(['index']);
    }





}
